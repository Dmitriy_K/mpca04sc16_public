#!/bin/bash
#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------
# mpca04sc16
#-------------------------------------------------------------------------------
#----
echo -e "\n"
echo -e "SETUP SCRIPT IS STARTED ----\n"

#----
echo -e "SETUP PYTHON:\n"
PATH=${PWD}/ver/run_tool/:$PATH
PYTHONPATH=${PWD}/ver/run_tool/classes/:$PYTHONPATH
PYTHONPATH=${PWD}/ver/run_tool/modules/:$PYTHONPATH
PYTHONPATH=${PWD}/ver/common/:$PYTHONPATH
LIBPYTHON_LOC=$(cocotb-config --libpython 2>/dev/null)
echo -e "\t$(python --version)"
echo -e "\tPATH:            \t${PATH}"
echo -e "\tPYTHONPATH:      \t${PYTHONPATH}"
echo -e "\tLIBPYTHON_LOC:   \t${LIBPYTHON_LOC}"
export PATH
export PYTHONPATH
export LIBPYTHON_LOC

#----
echo -e "SETUP RUN TOOL:\n"
alias run.py="python3 $(pwd)/ver/run_tool/run.py"
echo -e "\tRun tool source is $(pwd)/ver/run_tool/run.py\n"

#----
echo -e "SETUP SCRIPT IS FINISHED ----\n"
