//-------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
//------------ Urals the ridge
// mpca04sc16
// uncomments the necessary bunch of parameters
//-------------------------------------------------------------------------------
// Verilator
// --timescale 1ns/1ps
// -Wno-WIDTH


// Questa
-timescale 1ns/1ps
-mfcu
-sv17compat
-svinputport=relaxed