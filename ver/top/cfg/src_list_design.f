#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
+incdir+${SRC_DIR}
${SRC_DIR}/prj_func.sv
${SRC_DIR}/mpca04sc16_pkg.sv
${SRC_DIR}/mac_1ph.sv
${SRC_DIR}/fu.sv
${SRC_DIR}/du.sv
${SRC_DIR}/tag_gen.sv
${SRC_DIR}/mem_sr.sv
${SRC_DIR}/mux.sv
${SRC_DIR}/rar_str_mux.sv
${SRC_DIR}/rar_str.sv
${SRC_DIR}/rar_buf.sv
${SRC_DIR}/rar_nba_mux.sv
${SRC_DIR}/rar_exe_mux.sv
${SRC_DIR}/rar.sv
${SRC_DIR}/cpu.sv
