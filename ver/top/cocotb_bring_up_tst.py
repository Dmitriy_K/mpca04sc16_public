#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import random
from pathlib import Path

import pytest

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge, RisingEdge
from cocotb.triggers import Timer
from cocotb.log import logging
from cocotb.binary import BinaryValue

import sram
import code_gen
import fu_monitor

pytestmark = pytest.mark.simulator_required

#----
TIME_VALUE_INDX = 0
TIME_UNIT_INDX = 1
CLK_PRD = (1, "us")
PRE_START_TIME = (CLK_PRD[TIME_VALUE_INDX]/2, CLK_PRD[TIME_UNIT_INDX])
IMEM_SIZE = 128
DMEM_SIZE = 128
DATA_W = 16
NO_CLK_CYCLES = 100

#----
@cocotb.test()
async def bring_up_tst(dut):
    """A bring up test"""
    #----
    dut.i_i_dma_rdy_ch0.value = 1
    dut.i_i_dma_bp_ch0.value = 0

    instr_mem_port = {
        'rstn': dut.i_rst_n,
        'clk':  dut.i_clk,
        'cen':  dut.o_i_dma_cen_ch0,
        'oen':  '0',
        'wen':  'RO',
        'addr': dut.o_i_dma_addr_ch0,
        'di':   None,
        'do':   dut.i_i_dma_data_ch0
    }

    data_mem_port = {
        'rstn': dut.i_rst_n,
        'clk':  dut.i_clk,
        'cen':  dut.o_d_dma_cen_ch0,
        'oen':  '0',
        'wen':  dut.o_d_dma_wen_ch0,
        'addr': dut.o_d_dma_addr_ch0,
        'di':   dut.o_d_dma_data_ch0,
        'do':   dut.i_d_dma_data_ch0
    }

    fu0_tst_port = {
        'rst_n':            dut.i_rst_n,
        'clk':              dut.i_clk,
        'o_du_instr_rdy':   dut.fu0.o_du_instr_rdy,
        'o_du_bp':          dut.fu0.o_du_bp,
        'o_du_instr':       dut.fu0.o_du_instr
    }


    cocotb.log.info("Create data structures ------------------------------------")
    cmd_stream = []
    cmd_gen = code_gen.IMPLICACodeGen('mpca04sc16_cmd_gen')

    #----
    cocotb.log.info("Create components -----------------------------------------")
    clock = Clock(dut.i_clk, *CLK_PRD)
    clock.log.info("\t" + clock.__str__())
    #----
    data_mem = sram.SRAMModel("data_mem", data_mem_port, DATA_W, DMEM_SIZE)
    data_mem.log.info("\t" + data_mem.__str__())
    instr_mem = sram.SRAMModel("instr_mem", instr_mem_port, DATA_W, IMEM_SIZE)
    instr_mem.log.info("\t" + instr_mem.__str__())
    #----
    fu0_monitor = fu_monitor.FUResMonitor("fu0_mon", fu0_tst_port)
    fu0_monitor.log.info("\t" + fu0_monitor.__str__())

    #---- verbosity levels
    #DEBUG, INFO
    data_mem.log.setLevel(logging.INFO)
    instr_mem.log.setLevel(logging.INFO)
    fu0_monitor.log.setLevel(logging.INFO)
    cmd_gen.log.setLevel(logging.INFO)

    #----
    cocotb.log.info("Data initialization ---------------------------------------")
    cmd_stream = cmd_gen.generate_stream(IMEM_SIZE, 0)
    instr_mem_dump = cmd_gen.convert_to_memory_dump(cmd_stream)
    instr_mem.load(instr_mem_dump)
    fu0_monitor.set_stream_to_compare(cmd_stream, None)

    # cmd_gen.print_stream(1, cmd_stream)
    instr_mem.print_dump()

    #----
    cocotb.log.info("Run -------------------------------------------------------")
    await Timer(*PRE_START_TIME)
    dut.i_rst_n.value = 0
    dut.i_rst_n._log.info("\tReset is set")
    cocotb.start_soon(clock.start())
    cocotb.start_soon(data_mem.start())
    cocotb.start_soon(instr_mem.start())
    cocotb.start_soon(fu0_monitor.start())
    #----
    dut.i_ext_int.value = 0
    dut.i_tst_stop_fetch.value = 0
    #----
    for i in range(10):
        await RisingEdge(dut.i_clk)
    dut.i_rst_n.value = 1
    dut.i_rst_n._log.info("\tReset is released")

    for i in range(NO_CLK_CYCLES):
        await RisingEdge(dut.i_clk)

    fu0_monitor.print_summary(0)

if __name__ == "__main__":
    bring_up_tst()
