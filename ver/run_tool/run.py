#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import os.path
import sys
import re
import logging
import subprocess
from pprint import pprint
from time import localtime, strftime
import c_prj_settings
import help
import create_work_directory
import compile_design
import compile_test_bench
import simulation

#----
SETTINGS_FILE_PATH = os.path.abspath("./project.yaml")

#----
def prepare_cmds(args):
    curr_key = None
    commands = {}
    #----
    for item in args:
        if re.match(r'(?<!-)-(?!-)(\w+)', item) != None:
            curr_key = item
            commands[curr_key] = ""
        elif curr_key != None:
            commands[curr_key] = ("" if commands[curr_key] == "" else commands[curr_key] + " ") + item
    #----
    return commands

#----
def main(argv):
    start_time = strftime("%Y%m%d_%H%M%S", localtime())
    g_settings = c_prj_settings.c_prj_settings()
    mode = 'batch'
    coverage = False
    verbose_lvl = 'low'

    #----
    subprocess.call("clear", shell=True, stderr=subprocess.DEVNULL)
    print("\nSTART:\n")

    #---- prepare variables
    if g_settings.rd_settings(SETTINGS_FILE_PATH) != True:
        print("You are neither in a project nor in a work directory to proceed next actions. Please, move to any of them")
        return False

    #----
    arg_list = argv[1:]
    commands = prepare_cmds(arg_list)

    g_settings.print_vars()

    #---- non-sequenced commands
    if "-h" in commands:
        help.h(g_settings)
        return 0

    #----
    if "-cwd" in commands:
        if g_settings.is_run_dir() == False: create_work_directory.cwd(g_settings, commands["-cwd"])
        else: print(f"You can run the \'-cwd\' command only in a project directory. Move to there to try again")
        return 0

    #---- modificators
    if "-m" in commands:
        mode = commands["-m"]
        del commands["-m"]
    if "-cov" in commands:
        coverage = True
        del commands["-cov"]
    if "-vl" in commands:
        verbose_lvl = commands["-vl"]
        del commands["-vl"]

    #---- sequenced commands
    if g_settings.is_run_dir() == False:
        print(f"You can execute you command only in a work directory. Move to there and try again")
        return 1

    #----
    g_settings.put_env_vars()

    #----
    if "-swc" in commands:
        print(f'The option \'-swc\' is not implemented yet')
        del commands["-swc"]
    if "-gmc" in commands:
        print(f'The option \'-gmc\' is not implemented yet')
        del commands["-gmc"]
    if "-cdesign" in commands:
        if g_settings.get_var('CAD_PLATFORM') == 'questa':
            compile_design.questa(start_time, g_settings, commands["-cdesign"], mode, verbose_lvl)
        elif g_settings.get_var('CAD_PLATFORM') == 'verilator':
            compile_design.verilator(start_time, g_settings, commands["-cdesign"], verbose_lvl)
        else:
            logging.error("\t'%s' is the unsupported platform type for design compilation, make corrections to your 'project.yaml'", g_settings.get_var('CAD_PLATFORM'))
        del commands["-cdesign"]
    if "-ctb" in commands:
        if g_settings.get_var('CAD_PLATFORM') == 'questa':
            compile_test_bench.ctb(start_time, g_settings, commands["-ctb"], mode, coverage, verbose_lvl)
        else:
            logging.error("\t'%s' is the unsupported platform type for TB compilation, make corrections to your 'project.yaml'", g_settings.get_var('CAD_PLATFORM'))
        del commands["-ctb"]
    if "-sim" in commands:
        if g_settings.get_var('CAD_PLATFORM') == 'questa':
            simulation.questa(start_time, g_settings, commands["-sim"], mode, verbose_lvl)
        elif g_settings.get_var('CAD_PLATFORM') == 'verilator':
            simulation.verilator(start_time, g_settings, commands["-sim"], verbose_lvl)
        else:
            logging.error("\t'%s' is the unsupported platform type for design compilation, make corrections to your 'project.yaml'", g_settings.get_var('CAD_PLATFORM'))
        del commands["-sim"]
    if len(commands) > 0:
        print("There is no such commands:")
        for item in list(commands):
            print(f"\t{item}")

    #---- exit
    print("\nFINISH:\n")
    return 0

#----
if __name__ == '__main__':
    main(sys.argv)
