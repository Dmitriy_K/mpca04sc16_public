#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------
# mpca04sc16
#------------
# Creates a work directory
#-------------------------------------------------------------------------------

#----
def h(g_settings):
    rpt = "An EDA run tool.\n"
    rpt += "Usage:\n"
    rpt += "    run.py [-h|--help]\n"
    rpt += "    run.py -cwd unit=top dir=_work\n"
    rpt += "    run.py [-m|--mode]=batch -cdesign opts=lint -cc -ctb\n"
    rpt += "    run.py [-m|--mode]=batch -sim tn=tst_start -cov\n"
    rpt += "Options:\n"
    rpt += "    -cwd unit=<unit name> dir=<directory name>\n"
    rpt += "        creates work/run directory\n"
    rpt += "        <unit name>:\n"
    rpt += "            Set a unit name which is the same as a part of its TB directory name 'tb_<unit name>' and its test directory name <path>/vp/<unit name>.\n"
    rpt += "        <directory name>:\n"
    rpt += f"           Specify a name of your work directory as a subdirectory in the {g_settings.get_var('DFLT_RUN_DIR')}.\n"
    rpt += "    -m <options>\n"
    rpt += "        run mode\n"
    rpt += "        <options>:\n"
    rpt += "            batch  - run simulation in the batch mode (default)\n"
    rpt += "            dbg_ps - run simulation in the post-sim debugging mode\n"
    rpt += "            dbg_ls - run simulation in the live-sim debugging mode\n"
    rpt += "    -cov\n"
    rpt += "        run simulation in the coverage mode\n"
    rpt += "    -swc\n"
    rpt += "        compiles SW C/C++ sources\n"
    rpt += "    -gmc\n"
    rpt += f"       compiles 'Golden model' SW C/C++ sources. A sources list is {g_settings.get_var('CONF_DIR')}/src_list_gm.f\n"
    rpt += "    -cdesign opts=<option0>, <option1> args=<args>\n"
    rpt += "        design sources compilation\n"
    rpt += "        <opts>:\n"
    rpt += "            elab - elaborate design\n"
    rpt += "            lint - specify if linter/static code analysis will be run\n"
    rpt += "        <args>:\n"
    rpt += "            \"\" - put anyting, you want to provide directly to a compilation tool, between quotes\n"
    rpt += "    -ctb args=<args>\n"
    rpt += "        tb sources compilation and whole design optimization\n"
    rpt += "        <args>:\n"
    rpt += "            \"\" - put anyting, you want to provide directly to a compilation tool, between quotes\n"
    rpt += "    -sim tn=<test name> args=<args>\n"
    rpt += "        run simulation (if you want to use an existed database)\n"
    rpt += "        <test name> - specify the name of the test to run (required).\n"
    rpt += "        <args>:\n"
    rpt += "            \"\" - put anyting, you want to provide directly to a simulation tool, between quotes\n"
    rpt += "    -vl <options>\n"
    rpt += "        <options>:\n"
    rpt += "            low    show only errors and warnings (default)\n"
    rpt += "            high   show full output of tools\n"
    rpt += "    -h, --help\n"
    rpt += "        Print usage information.\n"

    print(rpt)