#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import os.path
import re
import subprocess

#----
def ctb(start_time, g_settings, args="", mode="batch", en_cov=False, vl="low"):
    # print(f'{args=}')
    TOP_MODULE              = g_settings.get_var('TB_TOP')
    RPT_DIR_NAME            = "./rpt/" + start_time
    DESIGN_COMPILER_LIB     = "design_lib"
    TB_COMPILER_LIB         = "tb_lib"
    ELAB_OTPUT              = "opt"
    COMPILER_LOG_NAME       = "ctb.log"
    COMPILER_GEN_OPTS_STR   = "-64 -sv +nospecify +notimingchecks"
    elaborator_log          = f"./rpt/{start_time}/elab.log"
    compiler_log            = f"./rpt/{start_time}/{COMPILER_LOG_NAME}"
    compiler_session_opts   = f"./rpt/{start_time}/tb_compiler_session_opts.out"
    elab_session_opts       = f"./rpt/{start_time}/tb_elab_session_opts.out"
    compiler_opts_str       = COMPILER_GEN_OPTS_STR + " " + \
        f"-f {g_settings.get_var('CAD_SETTINGS_TB_CMPL')} -f {g_settings.get_var('TB_SRC_LIST')} " + \
        f"-outf {compiler_session_opts} -work {TB_COMPILER_LIB}"
    elaborator_opts_str     = f"-64 -f {g_settings.get_var('CAD_SETTINGS_OPT')} " + \
        f"-o {ELAB_OTPUT} -outf {elab_session_opts} -L {TB_COMPILER_LIB} -L {DESIGN_COMPILER_LIB} -work $PWD/work {TOP_MODULE}"

    #---- prepare options
    args_line   = re.search(r'args=(.*)', args)
    args_line   = args_line.group(1) if args_line != None else ""

    compiler_opts_str = compiler_opts_str + " " + args_line
    if mode == "dbg_ps": elaborator_opts_str = elaborator_opts_str + " -debug +acc -debugdb"
    if mode == "dbg_ls": elaborator_opts_str = elaborator_opts_str + " -debug +acc=apvmnrt"

    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {compiler_log} $PWD/rpt/{COMPILER_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN TB FILES COMPILATION:\n")
    print(f'{compiler_opts_str=}')
    print(f'Mode: {mode}')
    print(f'Enable coverage: {en_cov}')
    print('')

    subprocess.call(f"vmap mtiUvm {g_settings.get_var('UVM_PATH')}", shell=True, stderr=subprocess.DEVNULL)
    subprocess.call(f"vlib -unlocklib work", shell=True, stderr=subprocess.DEVNULL)
    subprocess.call(f"vlog {compiler_opts_str} -l {compiler_log}", shell=True, stderr=subprocess.DEVNULL)

    print("\nRUN ELABORATION:\n")
    print(f'{elaborator_opts_str=}')
    print('')
    subprocess.call(f"vopt -64 {elaborator_opts_str} -l {elaborator_log}", shell=True, stderr=subprocess.DEVNULL)

    #----
    return 0
