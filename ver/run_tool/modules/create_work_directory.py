#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------
# mpca04sc16
#------------
# Creates a work directory
#-------------------------------------------------------------------------------
import os.path
import re
import subprocess

#----
def cwd(g_settings, args=""):
    unit_name   = re.search(r'unit=(\w+)', args)
    dir_name    = re.search(r'dir=(\w+)', args)
    unit_name   = "top" if unit_name == None else unit_name.group(1)
    dir_name    = f"_{unit_name}_wrk_dir_0" if dir_name == None else dir_name.group(1)
    dir_path    = os.path.abspath(g_settings.get_var('DFLT_RUN_DIR') + '/' + dir_name)

    #----
    try:
        created_dir = os.scandir(dir_path)
    except:
        os.makedirs(dir_path)
        print(f'The directory \'{dir_name}\' was created:')
        errors = False
    else:
        print(f'The directory {dir_name} is already exists. Please, change the name.')
        errors = True

    if errors == False:
        #---- replace parameters
        if g_settings.is_postponed_var() == True:
            g_settings.replace_postponed_var("UNIT_NAME", unit_name)
        g_settings.write_project_yaml(dir_path + "/project.yaml")
        #---- create directories
        os.makedirs(dir_path + "/rpt")
        #---- copy necessary test files
        #---- links to sources
        subprocess.call(f"ln -s {g_settings.get_var('SRC_DIR')} {dir_path}",
                        shell=True, stderr=subprocess.DEVNULL)
        subprocess.call(f"ln -s -T {g_settings.get_var('TSTS_DIR')} {dir_path}/tsts",
                        shell=True, stderr=subprocess.DEVNULL)
        #----
        # subprocess.call(f"cat {dir_path}/project.yaml", shell=True, stderr=subprocess.DEVNULL)
        subprocess.call(f"tree {dir_path}", shell=True, stderr=subprocess.DEVNULL)

    #----
    return errors