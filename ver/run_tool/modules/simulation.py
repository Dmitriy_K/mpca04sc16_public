#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
# see some tips there: https://docs.cocotb.org/en/stable/custom_flows.html
#-------------------------------------------------------------------------------
import os.path
import re
import subprocess

#----
def questa(start_time, g_settings, args="", mode="batch", vl="low"):
    COCOTB_LIB_DIR      = subprocess.run(['cocotb-config', '--lib-name-path', 'vpi', 'questa'], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
    TOP_MODULE          = g_settings.get_var('DESIGN_TOP') if g_settings.get_var('UNIT_NAME') == 'top' else g_settings.get_var('UNIT_NAME')
    RPT_DIR_NAME        = "./rpt/" + start_time
    ELAB_OTPUT          = "design_opt"
    SIM_LOG_NAME        = "sim.log"
    SIM_GEN_OPTS_STR    = f"-64 -pli {COCOTB_LIB_DIR} -f {g_settings.get_var('CAD_SETTINGS_SIM')}"
    sim_log             = f"./rpt/{start_time}/{SIM_LOG_NAME}"
    env_opts_str        = f"PYTHONPATH={g_settings.get_var('TSTS_DIR')}/:$PYTHONPATH" + \
                            f" TOPLEVEL={TOP_MODULE}" + \
                                f" TOPLEVEL_LANG=verilog" + \
                                    f" COCOTB_ANSI_OUTPUT=1"
    sim_opts_str        = SIM_GEN_OPTS_STR + f" -work work {ELAB_OTPUT}"

    #---- prepare options
    test_name   = re.search(r'tn=(\w+(\s*,+\s*\w+)*)+', args)
    args_line   = re.search(r'args=(.*)', args)
    test_name   = test_name.group(1) if test_name != None else ""
    args_line   = f" {args_line.group(1)}" if args_line != None else ""

    env_opts_str += f" MODULE={test_name}"
    sim_opts_str += args_line

    if mode == "batch":
        sim_opts_str += f" -batch -do \"onbreak resume; run -a; q -f;\""
    elif mode == "dbg_ps":
        sim_opts_str += f" -batch -postsimdataflow -wlf vsim.wlf"
        sim_opts_str += f" -do \"onbreak resume; run 0; add log -r /*; run -a; q -f;\""
    elif mode == "dbg_ls":
        sim_opts_str += f" -do \"onbreak resume; run 0; do wave.do;\""
    else:
        print("There is no '%s' mode, check your arguments", mode)
        return 0

    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {sim_log} $PWD/rpt/{SIM_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN SIMULATION:\n")
    print(f'{env_opts_str=}')
    print(f'{sim_opts_str=}')
    print(f'Mode: {mode}')
    print(f'Test name: {test_name}')
    print(f'')

    if test_name == "":
        print("Please, define a test name: 'run.py -sim tn=<test_name>'")
        return 1

    cmd2call = f"{env_opts_str}"
    cmd2call += f" vsim {sim_opts_str} -l {sim_log}"
    subprocess.call(cmd2call, shell=True)

    if mode == "dbg_ps":
        print("\nPOST SIMULATION ACTIONS:\n")
        print(f'')
        cmd2call = f"vsim -do \"dataset open vsim.wlf; do wave.do; wave zoomfull;\""
        subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)

    #----
    return 0

#----
def verilator(start_time, g_settings, args="", vl="low"):
    TOP_MODULE          = g_settings.get_var('DESIGN_TOP') if g_settings.get_var('UNIT_NAME') == 'top' else g_settings.get_var('UNIT_NAME')
    RPT_DIR_NAME        = "./rpt/" + start_time
    SIM_LOG_NAME        = "sim.log"
    CPPFLAGS            = f"CPPFLAGS=\"-std=c++11\""
    SIM_GEN_OPTS_STR    = ""
    sim_log             = f"./rpt/{start_time}/{SIM_LOG_NAME}"
    env_opts_str        = f"PYTHONPATH={g_settings.get_var('TSTS_DIR')}/:$PYTHONPATH" + \
                            f" TOPLEVEL={TOP_MODULE}" + \
                                f" TOPLEVEL_LANG=verilog" + \
                                    f" COCOTB_ANSI_OUTPUT=1"
    sim_opts_str        = SIM_GEN_OPTS_STR + f""

    #---- prepare options
    test_name   = re.search(r'tn=(\w+(\s*,+\s*\w+)*)+', args)
    args_line   = re.search(r'args=(.*)', args)
    test_name   = test_name.group(1) if test_name != None else ""
    args_line   = f" {args_line.group(1)}" if args_line != None else ""

    env_opts_str += f" MODULE={test_name}"
    sim_opts_str += args_line

    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {sim_log} $PWD/rpt/{SIM_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN SIMULATION:\n")
    print(f'{env_opts_str=}')
    print(f'{sim_opts_str=}')
    print(f'{CPPFLAGS=}')
    print(f'Test name: {test_name}')
    print(f'')

    if test_name == "":
        print("Please, define a test name: 'run.py -sim tn=<test_name>'")
        return 1


    cmd2call = f"{env_opts_str}"
    cmd2call += f" {CPPFLAGS} make {sim_opts_str} -f Vtop.mk"
    subprocess.call(cmd2call, shell=True)
    cmd2call = f"{env_opts_str}"
    cmd2call += f" ./Vtop |& tee {sim_log}"
    subprocess.call(cmd2call, shell=True)

    #----
    return 0
