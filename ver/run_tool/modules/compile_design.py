#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import os.path
import re
import subprocess

#----
def file_to_str(path) -> str:
    output = ""
    if os.path.isfile(path):
        with open(path, 'r') as fn:
            while (line := fn.readline().rstrip()):
                if not "#" in line:
                    output += line + " "
        return output

#----
def questa(start_time, g_settings, args="", mode="batch", vl="low"):
    # print(f'{args=}')
    TOP_MODULE              = g_settings.get_var('DESIGN_TOP') if g_settings.get_var('UNIT_NAME') == 'top' else g_settings.get_var('UNIT_NAME')
    RPT_DIR_NAME            = "./rpt/" + start_time
    COMPILER_LIB            = "design_lib"
    ELAB_OTPUT              = "design_opt"
    COMPILER_LOG_NAME       = "cdesign.log"
    COMPILER_GEN_OPTS_STR   = "-64 -sv +define+COCOTB_SIM"
    elaborator_log          = f"./rpt/{start_time}/elab.log"
    compiler_log            = f"./rpt/{start_time}/{COMPILER_LOG_NAME}"
    compiler_session_opts   = f"./rpt/{start_time}/design_compiler_session_opts.out"
    elab_session_opts       = f"./rpt/{start_time}/design_elab_session_opts.out"
    compiler_opts_str       = COMPILER_GEN_OPTS_STR + " " + \
        f"-f {g_settings.get_var('CAD_SETTINGS_DESIGN_CMPL')} -f {g_settings.get_var('DESIGN_SRC_LIST')} " + \
        f"-outf {compiler_session_opts} -work {COMPILER_LIB}"
    elaborator_opts_str     = f"-64 +acc -f {g_settings.get_var('CAD_SETTINGS_OPT')} " + \
        f"-o {ELAB_OTPUT} -outf {elab_session_opts} -L {COMPILER_LIB} -work work {TOP_MODULE}"

    #---- prepare options
    opts_line   = re.search(r'opts=(\w+(\s*,+\s*\w+)*)+', args)
    args_line   = re.search(r'args=(.*)', args)
    opts_line   = opts_line.group(1) if opts_line != None else ""
    args_line   = args_line.group(1) if args_line != None else ""

    # todo - leave condition operator after introducing the sim command
    en_lint = True if "lint" in opts_line else False
    en_elab = True
    # en_elab = True if "elab" in opts_line else False
    compiler_opts_str = compiler_opts_str + " " + args_line
    if mode == "dbg_ps": elaborator_opts_str = elaborator_opts_str + " -debug +acc -debugdb"
    if mode == "dbg_ls": elaborator_opts_str = elaborator_opts_str + " -debug +acc=apvmnrt"


    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {compiler_log} $PWD/rpt/{COMPILER_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN DESIGN FILES COMPILATION:\n")
    print(f'{compiler_opts_str=}')
    print(f'Enable lint: {en_lint}')
    print(f'Enable elaboration: {en_elab}')
    print(f'')

    cmd2call = "vlib -unlocklib work"
    subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)
    cmd2call = f"vlog {compiler_opts_str} -l {compiler_log}"
    subprocess.call(cmd2call, shell=True)

    if en_elab == True:
        print("\nRUN DESIGN INTERMEDIATE ELABORATION:\n")
        print(f'{elaborator_opts_str=}')
        print(f'')
        cmd2call = f"vopt {elaborator_opts_str} -l {elaborator_log}"
        subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)

    #----
    return 0

#----
def icarus(start_time, g_settings, args="", vl="low"):
    # print(f'{args=}')
    TOP_MODULE              = g_settings.get_var('DESIGN_TOP') if g_settings.get_var('UNIT_NAME') == 'top' else g_settings.get_var('UNIT_NAME')
    RPT_DIR_NAME            = "./rpt/" + start_time
    COMPILER_LIB            = "design_lib"
    COMPILER_LOG_NAME       = "cdesign.log"
    compiler_log            = f"./rpt/{start_time}/{COMPILER_LOG_NAME}"
    design_srcs_list        = file_to_str(g_settings.get_var('DESIGN_SRC_LIST'))
    compiler_opts_str       = f"-o {COMPILER_LIB}.vvp -D COCOTB_SIM=1 -f {g_settings.get_var('CAD_SETTINGS_DESIGN_CMPL')} -s {TOP_MODULE} -g2012 {design_srcs_list}"
    if vl == "high":
        compiler_opts_str += " -v"

    #---- prepare options
    opts_line   = re.search(r'opts=(\w+(\s*,+\s*\w+)*)+', args)
    args_line   = re.search(r'args=(.*)', args)
    opts_line   = opts_line.group(1) if opts_line != None else ""
    args_line   = args_line.group(1) if args_line != None else ""

    compiler_opts_str = compiler_opts_str + " " + args_line

    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {compiler_log} $PWD/rpt/{COMPILER_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN DESIGN FILES COMPILATION:\n")
    print(f'{compiler_opts_str=}')
    print(f'Enable lint: the linting stage is not available for Icarus')
    print(f'Enable elaboration: the optimization stage is not available for Icarus')
    print(f'')

    cmd2call = f"rm -rf {COMPILER_LIB}"
    subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)
    cmd2call = f"iverilog {compiler_opts_str} |& tee {compiler_log}"
    subprocess.call(f"echo {cmd2call}", shell=True, stderr=subprocess.DEVNULL)
    subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)

    #----
    return 0

#----
def verilator(start_time, g_settings, args="", vl="low"):
    # print(f'{args=}')
    TOP_MODULE              = g_settings.get_var('DESIGN_TOP') if g_settings.get_var('UNIT_NAME') == 'top' else g_settings.get_var('UNIT_NAME')
    RPT_DIR_NAME            = "./rpt/" + start_time
    COMPILER_LIB            = "design_lib"
    COMPILER_LOG_NAME       = "cdesign.log"
    COCOTB_PY_DIR           = subprocess.run(['cocotb-config', '--prefix'], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
    COCOTB_LIB_DIR          = subprocess.run(['cocotb-config', '--lib-dir'], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
    COCOTB_VERILATOR_CPP    = f"{COCOTB_PY_DIR}/cocotb/share/lib/verilator/verilator.cpp"
    compiler_log            = f"./rpt/{start_time}/{COMPILER_LOG_NAME}"
    vpi_cmpl_opts           = f"--vpi --public-flat-rw --prefix Vtop -o Vtop -LDFLAGS \"-Wl,-rpath,{COCOTB_LIB_DIR} -L{COCOTB_LIB_DIR} -lcocotbvpi_verilator\""
    compiler_opts_str       = f"-cc --exe -Mdir ./ -DCOCOTB_SIM=1 --top-module {TOP_MODULE} {vpi_cmpl_opts} -f {g_settings.get_var('CAD_SETTINGS_DESIGN_CMPL')} -f {g_settings.get_var('DESIGN_SRC_LIST')} {COCOTB_VERILATOR_CPP}"

    #---- prepare options
    opts_line   = re.search(r'opts=(\w+(\s*,+\s*\w+)*)+', args)
    args_line   = re.search(r'args=(.*)', args)
    opts_line   = opts_line.group(1) if opts_line != None else ""
    args_line   = args_line.group(1) if args_line != None else ""

    # en_lint = True if "lint" in opts_line else False
    compiler_opts_str = compiler_opts_str + " " + args_line

    #---- create directories
    if os.path.isdir(RPT_DIR_NAME) == False: os.makedirs(RPT_DIR_NAME)

    #---- create links
    subprocess.call(f"ln -frs {compiler_log} $PWD/rpt/{COMPILER_LOG_NAME}", shell=True, stderr=subprocess.DEVNULL)

    #---- run
    print("\nRUN DESIGN FILES COMPILATION:\n")
    print(f'{compiler_opts_str=}')
    print(f'Enable lint: the linting stage is not realized for Verilator')
    print(f'Enable elaboration: the optimization stage is not available for Verilator')
    print(f'')

    cmd2call = f"rm -rf {COMPILER_LIB}"
    subprocess.call(cmd2call, shell=True, stderr=subprocess.DEVNULL)
    cmd2call = f"verilator {compiler_opts_str} |& tee {compiler_log}"
    subprocess.call(cmd2call, shell=True)
    cmd2call = f"make -C ./ -f Vtop.mk"
    subprocess.call(cmd2call, shell=True)

    #----
    return 0
