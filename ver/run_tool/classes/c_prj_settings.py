#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------
# mpca04sc16
#-------------------------------------------------------------------------------
import os.path
import os
import re
import yaml
from pprint import pprint

class c_prj_settings(object):
    def __init__(self):
        self.prj_name = "Unknown"
        self.prj_vars = {}
    #----
    def rd_settings(self, path):
        prj_data = {}
        if os.path.isfile(path):
            fn = path
            with open(fn) as f: prj_data.update(yaml.safe_load(f))
            if prj_data.get("project") is None:
                print("Yaml file has wrong format!")
                return False
            self.prj_name = prj_data.get("project", {}).get("name")
            self.prj_vars = prj_data.get("project", {}).get("variables")
            self.prj_vars = self.replace_vars(self.prj_vars)
            return True
        else:
            return False
    #----
    def set_var(self, name, value):
        self.prj_vars[name] = value
    #----
    def get_var(self, name):
        return self.prj_vars[name]
    #----
    def put_env_vars(self):
        if self.is_postponed_var() == False:
            for key in self.prj_vars:
                if self.prj_vars[key] != None:
                    os.environ[key] = self.prj_vars[key]
            return True
        else:
            print("There are still postponed vars in settings. Change them with the 'replace_postponed_var' command.")
            return False
    #----
    def print_vars(self):
        print("    Project name: ", self.prj_name)
        for key in self.prj_vars:
            if self.prj_vars[key] != None: print(f"    {key}:\t{self.prj_vars[key]}")
    #----
    def is_run_dir(self):
        for key in self.prj_vars:
            if self.prj_vars[key] != None:
                if bool(re.findall(r'(?<!\$)\${1,2}(\w+)', self.prj_vars[key])):
                    return False
        return True
    #----
    def is_postponed_var(self):
        for key in self.prj_vars:
            if self.prj_vars[key] != None:
                if bool(re.findall(r'(?<!\$)\$\$(?!\$)(\w+)', self.prj_vars[key])):
                    return True
        return False
    #----
    def get_global_var(self, name):
        ret = self.prj_vars.get(name)
        if ret is None:
            ret = os.getenv(name)
        if ret is None:
            print("Can't find variable \"{}\" in project, environment!".format(name))
        return ret
    #----
    def replace_vars(self, to_replace, recursive=True) -> str or list or dict:
        if type(to_replace) is str:
            m = re.findall(r'(?<!\$)\$(?!\$)(\w+)', to_replace)
            if bool(m):
                for item in m:
                    if self.get_global_var(item) is not None:
                        to_replace = re.sub(r'(?<!\$)\$(?!\$)' + item, self.get_global_var(item), to_replace)
                        stop_flag = False
                    else:
                        stop_flag = True
                if recursive and not stop_flag:
                    to_replace = self.replace_vars(to_replace, True)
        elif type(to_replace) is list:
            for index, item in enumerate(to_replace):
                to_replace[index] = self.replace_vars(item, recursive)
        elif type(to_replace) is dict:
            for item in to_replace:
                to_replace[item] = self.replace_vars(to_replace[item], recursive)
        return to_replace
    #----
    def replace_postponed_var(self, var_name: str, replace_by):
        # print(f'{var_name=}')
        for key in self.prj_vars:
            if self.prj_vars[key] != None:
                self.prj_vars[key] = re.sub(r'(?<!\$)\$\$(?!\$)'+ var_name, replace_by, self.prj_vars[key])
    #----
    def write_project_yaml(self, path):
        new_yaml = {"project": {"name": self.prj_name, "variables": self.prj_vars}}
        #----
        try:
            new_yaml_file = open(path, "w")
        except IOError:
            print("Can't write to a file \"{}\"".format(path))
            os.abort()
        else:
            yaml.dump(new_yaml, new_yaml_file)
        finally:
            new_yaml_file.close()
