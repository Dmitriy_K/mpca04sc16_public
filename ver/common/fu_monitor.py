#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
from cocotb.log import SimLog
from cocotb.triggers import RisingEdge
from cocotb.utils import lazy_property
import code_gen

#----
class FUResMonitor:
    """An FU output monitor"""
    def __init__(self, inst_name: str, port: dict):
        self.rpt = []
        self.is_err = False
        self.cmd_stream = []
        self.port = {}
        self.inst_name = inst_name
        self.port["rst_n"] = port["rst_n"]
        self.port["clk"] = port["clk"]
        self.port["i_stop_fetch"] = port["i_stop_fetch"]
        self.port["o_du_instr_rdy"] = port["o_du_instr_rdy"]
        self.port["o_du_bp"] = port["o_du_bp"]
        self.port["o_du_instr"] = port["o_du_instr"]
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'cocotb.{type(self).__qualname__}({self.inst_name})')
    #----
    def __str__(self):
        return self.inst_name + f'({type(self).__qualname__})'
    #----
    async def start(self):
        """The main FU monitor routine"""
        ref_list_indx = 0
        curr_cmd = code_gen.IMPLICACommand()
        while True:
            await RisingEdge(self.port["clk"])
            self.log.debug("\tFU monitor senses a clock rising edge")
            if self.port["rst_n"].value.binstr == '1':
                self.log.debug("\tFU monitor is in active mode")
                if self.port["o_du_instr_rdy"].value.binstr == '1' \
                    and self.port["i_stop_fetch"].value.binstr == '0':
                    self.log.debug("\tFU got a new command")
                    bp = self.port["o_du_bp"].value
                    raw_cmd = self.port["o_du_instr"].value
                    self.log.debug("\tFU monitor got command %x", raw_cmd)
                    curr_cmd.set_cmd(None, raw_cmd)
                    #----
                    if len(self.cmd_stream) > 0 and ref_list_indx < len(self.cmd_stream):
                        rpt = curr_cmd.print_cmd(0, 'string') + f"\t: (c) #{ref_list_indx:<6} "
                        if self.cmd_compare(curr_cmd, self.cmd_stream[ref_list_indx]):
                            rpt += " OK"
                        else:
                            rpt += " FAIL"
                            self.is_err = True
                        rpt += "\n" + self.cmd_stream[ref_list_indx].print_cmd(0, 'string') + f"\t: (r)"
                        ref_list_indx += 1
                        self.rpt.append(rpt)
                    else:
                        self.log.error("\tNo reference value was provided")
    #----
    def set_stream_to_compare(self, cmd_stream: list, bp_stream: list):
        """
        sets reference value(s) to compare
        """
        if isinstance(cmd_stream, list):
            self.cmd_stream = cmd_stream
            self.log.info("\tA command cmd_stream length of %d is loaded" % len(self.cmd_stream))
        else:
            self.log.error("\tProvided variable is not a 'list' type but a '%s' type" % type(cmd_stream))
        #----
        if bp_stream is None:
            self.log.info("\tA break point stream was not provided")
        elif isinstance(bp_stream, list):
            self.bp_stream = bp_stream
            self.log.info("\tA BP stream length of %d is loaded" % len(self.bp_stream))
        else:
            self.log.error("\tProvided variable is not a 'list' type but a '%s' type" % type(bp_stream))
    #----
    def cmd_compare(self, curr_cmd: code_gen.IMPLICACommand, ref_cmd: code_gen.IMPLICACommand):
        """
        compares a DUT out sample and a reference value
        """
        if  curr_cmd.fields['format']                == ref_cmd.fields['format'] \
            and curr_cmd.fields['eol']               == ref_cmd.fields['eol'] \
            and curr_cmd.fields['operation']         == ref_cmd.fields['operation'] \
            and curr_cmd.fields['am_arg1']           == ref_cmd.fields['am_arg1'] \
            and curr_cmd.fields['am_ref_base_arg1']  == ref_cmd.fields['am_ref_base_arg1'] \
            and curr_cmd.fields['am_bias_arg1']      == ref_cmd.fields['am_bias_arg1'] \
            and curr_cmd.fields['ss_arg0']           == ref_cmd.fields['ss_arg0'] \
            and curr_cmd.fields['ss_arg1']           == ref_cmd.fields['ss_arg1'] \
            and curr_cmd.fields['ss_ref_base_arg1']  == ref_cmd.fields['ss_ref_base_arg1'] \
            and curr_cmd.fields['ss_bias_arg1']      == ref_cmd.fields['ss_bias_arg1'] \
            and curr_cmd.fields['field0']            == ref_cmd.fields['field0'] \
            and curr_cmd.fields['field1']            == ref_cmd.fields['field1']:
            return 1
        else:
            return 0
    #----
    def print_summary(self, mode: int):
        """
        Prints results of comparison
            0 - a full report (only errors)
            1 - a full report (all)
            2 - a recent comparison
        """
        print("\n" + self.__str__() + " report **********:")
        code_gen.IMPLICACommand.print_header(0, 'printing')
        #----
        if mode == 2:
            print(self.rpt[-1])
        else:
            for item in self.rpt:
                if mode == 0 and 'FAIL' in item:
                    print(item)
                elif mode == 1:
                    print(item)
            if self.is_err == False:
                print(f"There are no FU failures in the test\n")
        print("")
