#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import random
import logging
from cocotb.log import SimLog
from cocotb.utils import lazy_property
from cocotb.binary import BinaryValue
from enum import Enum

#---- todo: move the next enumerates to a separate module, because it is general data types
#----
class IMPLICA_RAR_STR_STATES(Enum):
    EMPTY = 0
    WAIT = 1
    SET = 2
    RES = 3
#----
class IMPLICA_CND_STATES(Enum):
    CF = 0
    OVD = 1
    NZ = 2
    Z = 3
    NCEF = 4
    CEF = 5
    NA = 6
#----
class IMPLICA_EXE_TYPES(Enum):
    ALU = 0
    MAC = 1
    CU = 2
    MUL = 3
    NA = 4
#----
class IMPLICA_ADDR_MODES(Enum):
    DIR = 0
    IND = 1
    DISP = 2
    NA = 3
#----
class IMPLICA_SRCS(Enum):
    RAR = 0
    CG = 1
    MEM = 2
    CALC = 3
    NA = 4

#----
CMD_WRD_W = 16
ISA_OP_INDX = 0
ISA_FRMT_INDX = 1
ISA_EXE_INDX = 2
ISA_ACCESS_INDX = 3

IMPLICA_ISA_OP = {
    "NOP":   (0b11001, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "RR":    (0b11010, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "SYS":   (0b11011, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "EOL":   (0b11100, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "EXA":   (0b11101, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "CALL":  (0b11110, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "RET":   (0b11111, 'F0', IMPLICA_EXE_TYPES.CU.name,  'NA'),
    "ADD":   (0b01000, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "ADDC":  (0b01001, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "SUB":   (0b01010, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "SUBB":  (0b01011, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "BC":    (0b01100, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "BSM":   (0b01101, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "BSL":   (0b01110, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "CSL":   (0b01111, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "CSR":   (0b10000, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "LSL":   (0b10001, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "LSR":   (0b10010, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "ASL":   (0b10011, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "ASR":   (0b10100, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "AND":   (0b10101, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "OR":    (0b10110, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "XOR":   (0b10111, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "NOT":   (0b11000, 'F0', IMPLICA_EXE_TYPES.ALU.name, 'NA'),
    "MULL":  (0b00110, 'F0', IMPLICA_EXE_TYPES.MUL.name, 'NA'),
    "MULH":  (0b00111, 'F0', IMPLICA_EXE_TYPES.MUL.name, 'NA'),
    "LDUB":  (0b00000, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'RD'),
    "LDSB":  (0b00001, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'RD'),
    "LDU2B": (0b00010, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'RD'),
    "LDS2B": (0b00011, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'RD'),
    "STB":   (0b00100, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'WR'),
    "ST2B":  (0b00101, 'F1', IMPLICA_EXE_TYPES.MAC.name, 'WR'),
    "LDC":   (0b00000, 'F2', IMPLICA_EXE_TYPES.CU.name,  'NA')
}

#----
class IMPLICACommand:
    """
    A command for the IMPLICA series CPUs
    Command fields:
        - format
        - eol
        - operation
        - am_arg0
        - am_arg1
        - am_ref_base_arg1
        - am_bias_arg1
        - ss_arg0
        - ss_arg1
        - ss_ref_base_arg1
        - ss_bias_arg1
        - field0
        - field1
    """
    #----
    def __init__(self):
        self.fields = {}
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'{type(self).__qualname__}')
    #----
    def set_cmd(self, frmt: int, fields):
        """Sets internal fields"""
        #----
        if fields is None:
            if frmt is None:
                self.log.error("\tIf you do not provide any fields you must provide the format of the command")
            else:
                self.fields['format'] = frmt
                self.randomize(frmt, tuple)
        elif isinstance(fields, dict):
            self.fields |= fields
        elif isinstance(fields, tuple):
            self.fields |= self.get_decoded_cmd_word(fields)
        elif isinstance(fields, int):
            if fields & 0x3 == 0:
                pre_decoded_fields = (
                    fields >> 15,
                    (fields >> 10) & 0x1f,
                    (fields >> 6) & 0xf,
                    (fields >> 2) & 0xf,
                    fields & 0x3
                )
            elif fields & 0x3 == 1:
                pre_decoded_fields = (
                    fields >> 15,
                    (fields >> 13) & 0x3,
                    (fields >> 10) & 0x7,
                    (fields >> 6) & 0xf,
                    (fields >> 2) & 0xf,
                    fields  & 0x3
                )
            elif fields & 0x3 == 2:
                pre_decoded_fields = (
                    fields >> 2,
                    fields & 0x3
                )
            else:
                self.log.error("\tThe format #%d is unsupported" % (fields & 0x3))
            self.fields |= self.get_decoded_cmd_word(pre_decoded_fields)
            # print(pre_decoded_fields)
        else:
            self.log.error("\tUnsupported type(%s) of input data for initialization" % type(fields))
    #----
    def get_encoded_cmd_word(self, output_type: type=tuple):
        """
        Returns an encoded command word
            output_type: tuple, BinaryValue (packed command word)
        """
        if output_type is tuple:
            if self.fields['format'] == 0:
                return (self.fields['ss_arg1'], self.get_op_code(self.fields['operation']),
                        self.fields['field0'], self.fields['field1'],
                        self.fields['format'])
            elif self.fields['format'] == 1:
                return (self.fields['eol'], self.get_encoded_ss_am_field(),
                        self.get_op_code(self.fields['operation']), self.fields['field0'],
                        self.fields['field1'], self.fields['format'])
            elif self.fields['format'] == 2:
                return (self.fields['field1'], self.fields['format'])
        elif output_type == BinaryValue:
            if self.fields['format'] == 0:
                return BinaryValue(IMPLICA_SRCS[self.fields['ss_arg1']].value << 15 | self.get_op_code(self.fields['operation']) << 10
                        | self.fields['field0'] << 6 | self.fields['field1'] << 2
                        | self.fields['format'], CMD_WRD_W, False)
            elif self.fields['format'] == 1:
                return BinaryValue(self.fields['eol'] << 15 | self.get_encoded_ss_am_field() << 13
                        | (self.get_op_code(self.fields['operation']) & 0x7) << 10 | self.fields['field0'] << 6
                        | self.fields['field1'] << 2 | self.fields['format'], CMD_WRD_W, False)
            elif self.fields['format'] == 2:
                return BinaryValue(self.fields['field1'] << 2 | self.fields['format'], CMD_WRD_W, False)
        else:
            self.log.error("\t%s is an unsupported output type" % output_type)
    #----
    def get_decoded_cmd_word(self, fields: tuple) -> dict:
        """
        Returns a dictionary with command fields
        """
        result = {}
        #----
        if fields[-1] == 0:
            result['format'] = fields[4]
            result['eol'] = 0
            result['operation'] = self.get_op_abbreviation(fields[1], fields[4])
            result['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
            result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
            result['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
            result['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
            result['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            result['field0'] = fields[2]
            result['field1'] = fields[3]
            #----
            if fields[0] == 0:
                result['ss_arg1'] = IMPLICA_SRCS.RAR.name
            else:
                result['ss_arg1'] = IMPLICA_SRCS.CG.name
            #----
            if fields[2] == 0:
                result['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
                result['ss_arg0'] = IMPLICA_SRCS.NA.name
            else:
                result['am_arg0'] = IMPLICA_ADDR_MODES.DIR.name
                result['ss_arg0'] = IMPLICA_SRCS.RAR.name
        elif fields[-1] == 1:
            operation = self.get_op_abbreviation(fields[2], fields[5])
            #----
            result['format'] = fields[5]
            result['eol'] = fields[0]
            result['operation'] = self.get_op_abbreviation(fields[2], fields[5])
            result['field0'] = fields[3]
            result['field1'] = fields[4]
            #----
            if self.is_rd_cmd(operation) is True:
                result['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
                result['ss_arg0'] = IMPLICA_SRCS.NA.name
            else:
                result['am_arg0'] = IMPLICA_ADDR_MODES.DIR.name
                result['ss_arg0'] = IMPLICA_SRCS.RAR.name
            #----
            if fields[1] == 0:
                result['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
                result['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
                result['ss_arg1'] = IMPLICA_SRCS.RAR.name
                result['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
                result['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            elif fields[1] == 1:
                result['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
                result['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
                result['ss_arg1'] = IMPLICA_SRCS.CG.name
                result['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
                result['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            elif fields[1] == 2:
                result['am_arg1'] = IMPLICA_ADDR_MODES.DISP.name
                result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['am_bias_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['ss_arg1'] = IMPLICA_SRCS.CALC.name
                result['ss_ref_base_arg1'] = IMPLICA_SRCS.RAR.name
                result['ss_bias_arg1'] = IMPLICA_SRCS.CG.name
            elif fields[1] == 3:
                result['am_arg1'] = IMPLICA_ADDR_MODES.DISP.name
                result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['am_bias_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                result['ss_arg1'] = IMPLICA_SRCS.CALC.name
                result['ss_ref_base_arg1'] = IMPLICA_SRCS.MEM.name
                result['ss_bias_arg1'] = IMPLICA_SRCS.CG.name
        elif fields[-1] == 2:
            result['format'] = fields[1]
            result['eol'] = 0
            result['operation'] = self.get_op_abbreviation(0, fields[1])
            result['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
            result['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
            result['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
            result['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
            result['ss_arg0'] = IMPLICA_SRCS.NA.name
            result['ss_arg1'] = IMPLICA_SRCS.CG.name
            result['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
            result['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            result['field0'] = 0
            result['field1'] = fields[0]
        else:
            self.log.error("\tThe format #%d is unsupported" % self.fields['format'])
        #----
        return result
    #----
    def get_encoded_ss_am_field(self) -> int:
        """Returns encoded AM and SRC for current fields"""
        if self.fields['am_arg1'] == IMPLICA_ADDR_MODES.DIR.name \
            and self.fields['ss_arg1'] == IMPLICA_SRCS.RAR.name:
            return 0
        elif self.fields['am_arg1'] == IMPLICA_ADDR_MODES.DIR.name \
            and self.fields['ss_arg1'] == IMPLICA_SRCS.CG.name:
            return 1
        elif self.fields['am_arg1'] == IMPLICA_ADDR_MODES.DISP.name \
            and self.fields['ss_ref_base_arg1'] == IMPLICA_SRCS.RAR.name \
            and self.fields['ss_bias_arg1'] == IMPLICA_SRCS.CG.name:
            return 2
        elif self.fields['am_arg1'] == IMPLICA_ADDR_MODES.DISP.name \
            and self.fields['ss_ref_base_arg1'] == IMPLICA_SRCS.MEM.name \
            and self.fields['ss_bias_arg1'] == IMPLICA_SRCS.CG.name:
            return 3
        else:
            self.log.error("\tUnable to encode unsupported combination of command fields")
            for key, value in self.fields.items():
                print(f"\t{key}: {value}")
    #----
    @staticmethod
    def get_op_abbreviation(op: int, frmt: int=None, exe: str=None) -> str:
        """Returns an operation acronym"""
        for key, value in IMPLICA_ISA_OP.items():
            if value[ISA_OP_INDX] == op:
                if (frmt is not None and value[ISA_FRMT_INDX] == f'F{frmt}') \
                    or (exe is not None and value[ISA_EXE_INDX] == exe):
                    return key
    #----
    @staticmethod
    def get_op_code(op: str) -> int:
        """Returns an operation digital code"""
        value = IMPLICA_ISA_OP[op]
        return value[0]
    #----
    def is_rd_cmd(self, op):
        """Checks if it is a RD command"""
        if isinstance(op, str):
            item = IMPLICA_ISA_OP.get(op)
            return True if item[ISA_FRMT_INDX] == 'F1' and item[ISA_ACCESS_INDX] == 'RD' else False
        elif isinstance(op, tuple):
            return True if op[ISA_FRMT_INDX] == 'F1' and op[ISA_ACCESS_INDX] == 'RD' else False
        else:
            self.log.error("\tThe type(%s) of the input value is unsupported" % type(op))
            return None
    #----
    def randomize(self, frmt: int, fields_preset: dict=None):
        """Generates a random command word taking in account the preset"""
        op_list = []
        self.fields['format'] = frmt
        for key, value in IMPLICA_ISA_OP.items():
            if value[1] == f'F{frmt}':
                op_list.append(key)
        self.log.debug(op_list)
        self.fields['operation'] = random.choice(op_list)
        #----
        if frmt == 0:
            self.fields['eol'] = 0
            self.fields['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
            self.fields['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
            self.fields['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
            self.fields['ss_arg1'] = random.choice([IMPLICA_SRCS.CG.name, IMPLICA_SRCS.RAR.name])
            self.fields['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
            self.fields['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            self.fields['field0'] = random.randint(0, 15)
            self.fields['field1'] = random.randint(0, 15)
            if self.fields['field0'] == 0:
                self.fields['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
                self.fields['ss_arg0'] = IMPLICA_SRCS.NA.name
            else:
                self.fields['am_arg0'] = IMPLICA_ADDR_MODES.DIR.name
                self.fields['ss_arg0'] = IMPLICA_SRCS.RAR.name
        elif frmt == 1:
            self.fields['eol'] = random.randint(0, 1) if fields_preset['eol'] == None else fields_preset['eol']
            self.fields['field0'] = random.randint(0, 15)
            self.fields['field1'] = random.randint(0, 15)
            self.fields['am_arg1'] = random.choice([IMPLICA_ADDR_MODES.DIR.name, IMPLICA_ADDR_MODES.DISP.name])
            #----
            if self.is_rd_cmd(self.fields['operation']) is True:
                self.fields['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
                self.fields['ss_arg0'] = IMPLICA_SRCS.NA.name
            else:
                self.fields['am_arg0'] = IMPLICA_ADDR_MODES.DIR.name
                self.fields['ss_arg0'] = IMPLICA_SRCS.RAR.name
            #----
            if self.fields['am_arg1'] == IMPLICA_ADDR_MODES.DIR.name:
                self.fields['ss_arg1'] = random.choice([IMPLICA_SRCS.CG.name, IMPLICA_SRCS.RAR.name])
                self.fields['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
                self.fields['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
                self.fields['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
                self.fields['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
            else:
                self.fields['ss_arg1'] = IMPLICA_SRCS.CALC.name
                self.fields['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                self.fields['am_bias_arg1'] = IMPLICA_ADDR_MODES.DIR.name
                self.fields['ss_ref_base_arg1'] = random.choice([IMPLICA_SRCS.MEM.name, IMPLICA_SRCS.RAR.name])
                self.fields['ss_bias_arg1'] = IMPLICA_SRCS.CG.name
        elif frmt == 2:
            self.fields['eol'] = 0
            self.fields['field0'] = 0
            self.fields['field1'] = random.randint(0, 16384)
            self.fields['am_arg0'] = IMPLICA_ADDR_MODES.NA.name
            self.fields['am_arg1'] = IMPLICA_ADDR_MODES.DIR.name
            self.fields['am_ref_base_arg1'] = IMPLICA_ADDR_MODES.NA.name
            self.fields['am_bias_arg1'] = IMPLICA_ADDR_MODES.NA.name
            self.fields['ss_arg0'] = IMPLICA_SRCS.NA.name
            self.fields['ss_arg1'] = IMPLICA_SRCS.CG.name
            self.fields['ss_ref_base_arg1'] = IMPLICA_SRCS.NA.name
            self.fields['ss_bias_arg1'] = IMPLICA_SRCS.NA.name
        else:
            self.log.warning("\t%d is unsupported command format" % frmt)
    #----
    @staticmethod
    def print_header(representation: int, result: str='printing'):
        """
        Prints a table header for command rows
            representation: 0 - IMPLICACommand, 1 - BinaryValue
            result: 'printing', 'string'
        """
        if representation == 0:
            rpt_str = \
                  "\n |- frmt"\
                + "\n |  |- eol"\
                + "\n |  | |- op"\
                + "\n |  | |     |- arg0 AM"\
                + "\n |  | |     |    |- arg1 AM"\
                + "\n |  | |     |    |    |- arg1 ref base AM"\
                + "\n |  | |     |    |    |    |- arg1 bias AM"\
                + "\n |  | |     |    |    |    |    |- arg0 SRC"\
                + "\n |  | |     |    |    |    |    |    |- arg1 SRC"\
                + "\n |  | |     |    |    |    |    |    |    |- arg1 ref base SRC"\
                + "\n |  | |     |    |    |    |    |    |    |   |- arg1 bias SRC"\
                + "\n |  | |     |    |    |    |    |    |    |   |   |- f0"\
                + "\n |  | |     |    |    |    |    |    |    |   |   |  |- f1"\
                + "\n v  v v     v    v    v    v    v    v    v   v   v  v"
                #     2  1 5     4    4    4    4    4    4    3   3   2  5 - field width (one whitespace after is not included)
        elif representation == 1:
            rpt_str = \
                  "\n |- eol"\
                + "\n | |- arg1 AM and SRC code (0 - (RAR)/RAR, 1 - (RAR)/CG, 2 - (RAR)/RAR+CG, 3 - (RAR)/MEM+CG)"\
                + "\n | | |- arg1 SRC (0 - RAR, 1 - CG)"\
                + "\n | | | |- op"\
                + "\n | | | |  |- f0"\
                + "\n | | | |  | |- f1"\
                + "\n | | | |  | |    |-frmt"\
                + "\n v v v v  v v    v"
                #     1 1 1 2  1 4    1 - field width (one whitespace after is not included)
        else:
            logging.error("\t%s. '%s' is an unsupported output type", IMPLICACommand.__qualname__, representation)
            return
        #----
        if result == 'printing':
            print(rpt_str)
        else:
            return(rpt_str)
    #----
    def print_cmd(self, representation: int, result: str='printing'):
        """
        Prints command in a table format
            representation: 0 - IMPLICACommand, 1 - BinaryValue
            result: 'printing', 'string'
        """
        if representation == 0:
            rpt_str =  f" {self.fields['format']:<2}"           if self.fields['format'] is not None else " --"
            rpt_str += f" {self.fields['eol']:<1}"              if self.fields['eol'] is not None else " -"
            rpt_str += f" {self.fields['operation']:<5}"        if self.fields['operation'] is not None else " -----"
            rpt_str += f" {self.fields['am_arg0']:<4}"          if self.fields['am_arg0'] is not None else " ----"
            rpt_str += f" {self.fields['am_arg1']:<4}"          if self.fields['am_arg1'] is not None else " ----"
            rpt_str += f" {self.fields['am_ref_base_arg1']:<4}" if self.fields['am_ref_base_arg1'] is not None else " ----"
            rpt_str += f" {self.fields['am_bias_arg1']:<4}"     if self.fields['am_bias_arg1'] is not None else " ----"
            rpt_str += f" {self.fields['ss_arg0']:<4}"          if self.fields['ss_arg0'] is not None else " ----"
            rpt_str += f" {self.fields['ss_arg1']:<4}"          if self.fields['ss_arg1'] is not None else " ----"
            rpt_str += f" {self.fields['ss_ref_base_arg1']:<3}" if self.fields['ss_ref_base_arg1'] is not None else " ---"
            rpt_str += f" {self.fields['ss_bias_arg1']:<3}"     if self.fields['ss_bias_arg1'] is not None else " ---"
            rpt_str += f" {self.fields['field0']:<2}"           if self.fields['field0'] is not None else " --"
            rpt_str += f" {self.fields['field1']:<5}"           if self.fields['field1'] is not None else " -----"
        elif representation == 1:
            cmd = self.get_encoded_cmd_word(tuple)
            if self.fields['format'] == 0:
                rpt_str = \
                      f" -" \
                    + f" -" \
                    + f" {IMPLICA_SRCS[cmd[0]].value:<1}" \
                    + f" {cmd[1]:<2x}" \
                    + f" {cmd[2]:<1x}" \
                    + f" {cmd[3]:<4x}" \
                    + f" {cmd[4]:<1}"
            elif self.fields['format'] == 1:
                rpt_str = \
                      f" {cmd[0]:<1}" \
                    + f" {cmd[1]:<1}" \
                    + f" -" \
                    + f" {cmd[2]:<2x}" \
                    + f" {cmd[3]:<1x}" \
                    + f" {cmd[4]:<4x}" \
                    + f" {cmd[5]:<1}"
            elif self.fields['format'] == 2:
                rpt_str = \
                      f" -" \
                    + f" -" \
                    + f" -" \
                    + f" --" \
                    + f" -" \
                    + f" {cmd[0]:<4x}" \
                    + f" {cmd[1]:<1}"
        else:
            logging.error("\t%s. '%s' is an unsupported output type", IMPLICACommand.__qualname__, representation)
            return
        #----
        if result == 'printing':
            print(rpt_str)
        else:
            return(rpt_str)
#----
class IMPLICACodeGen:
    """A code generator for the IMPLICA series CPUs"""
    def __init__(self, inst_name: str, model: str = 'mpca04sc16'):
        self.model = model
        self.inst_name = inst_name
        self.cmd_stream = []
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'{type(self).__qualname__}({self.inst_name})')
    #----
    def __str__(self):
        return self.inst_name + f'({type(self).__qualname__}({self.model}))'
    #----
    def generate_stream(self, noc: int, frmt: int=None, fields_preset: dict=None) -> list:
        """
        Generates a stream of IMPLICA series CPUs commands
            - noc           - number of commands to generate
            - frmt          - format of commands to generate, None - is mix
            - fields_preset - predefined command fields
        """
        for i in range(noc):
            cmd = IMPLICACommand()
            cmd.randomize(random.randint(0, 2) if frmt is None else frmt, fields_preset)
            self.cmd_stream.append(cmd)
        return self.cmd_stream
    #----
    def convert_to_memory_dump(self, cmd_stream: list=None) -> list:
        """
        Converts to memory dump (BinaryValue)
        """
        cmds = []
        if cmd_stream is not None and len(self.cmd_stream) != 0:
            for item in self.cmd_stream:
                cmds.append(item.get_encoded_cmd_word(BinaryValue))
        return cmds
    #----
    def print_stream(self, representation: int, cmd_stream: list=None):
        """
        Print a stream of IMPLICA series CPUs commands
            representation:
                0 - all fields
                1 - encoded word

            1. if a stream is provided then it prints it
            2. if a stream is not provided then it prints the last generated stream

        """
        IMPLICACommand.print_header(representation, 'printing')
        if cmd_stream is not None and len(self.cmd_stream) != 0:
            for item in self.cmd_stream:
                item.print_cmd(representation, 'printing')
        else:
            self.log.error("\tNo stream provided or the stream is empty")