#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import cocotb
from cocotb.log import SimLog
from cocotb.triggers import RisingEdge
from cocotb.binary import BinaryValue
from cocotb.utils import lazy_property
import code_gen

#----
class RARstr:
    """
    A RAR string
    Active fields:
        - st
        - tag
        - exn
        - op
        - eol
        - bp
        - ss_arg0
        - ss_arg1
        - res_fl
        - arg0
        - arg1_res
    """
    #----
    def __init__(self, fields: dict = None):
        self.fields = {}
        if fields is not None and isinstance(fields, dict):
            self.fields |= fields
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'{type(self).__qualname__}')
    #----
    def set(self, fields):
        """Sets internal fields"""
        #----
        if fields is None:
            self.log.error("\tNo input variable to set the RAR string was provided")
        if isinstance(fields, dict):
            self.fields |= fields
        else:
            self.log.error("\tUnsupported type(%s) of input data for initialization" % type(fields))
    #----
    @staticmethod
    def print_header(result: str='printing'):
        """
        Prints a table header for string rows
            result: 'printing', 'string'
        """
        rpt_str = \
              "\n |- tag"\
            + "\n |  |- st"\
            + "\n |  |     |- bp"\
            + "\n |  |     | |- eol"\
            + "\n |  |     | | |- exn"\
            + "\n |  |     | | |   |- op"\
            + "\n |  |     | | |   |     |- res_fl"\
            + "\n |  |     | | |   |     |    |- ss_arg0"\
            + "\n |  |     | | |   |     |    |    |- ss_arg1"\
            + "\n |  |     | | |   |     |    |    |    |- arg0"\
            + "\n |  |     | | |   |     |    |    |    |     |- arg1_res"\
            + "\n v  v     v v v   v     v    v    v    v     v"
            #     2  5     1 1 3   5     4    4    4    5     5 - field width (one whitespace after is not included)
        #----
        if result == 'printing':
            print(rpt_str)
        else:
            return(rpt_str)
    #----
    def print_str(self, result: str='printing'):
        """
        Prints a RAR string
            result: 'printing', 'string'
        """
        rpt_str =  f" {self.fields['tag']:<2}"      if self.fields['tag'] is not None else " --"
        rpt_str += f" {self.fields['st']:<5}"       if self.fields['st'] is not None else " -----"
        rpt_str += f" {self.fields['bp']:<1}"       if self.fields['bp'] is not None else " -"
        rpt_str += f" {self.fields['eol']:<1}"      if self.fields['eol'] is not None else " -"
        rpt_str += f" {self.fields['exn']:<3}"      if self.fields['exn'] is not None else " ---"
        rpt_str += f" {self.fields['op']:<5}"       if self.fields['op'] is not None else " -----"
        rpt_str += f" {self.fields['res_fl']:<4}"   if self.fields['res_fl'] is not None else " ----"
        rpt_str += f" {self.fields['ss_arg0']:<4}"  if self.fields['ss_arg0'] is not None else " ----"
        rpt_str += f" {self.fields['ss_arg1']:<4}"  if self.fields['ss_arg1'] is not None else " ----"
        rpt_str += f" {self.fields['arg0']:<5}"     if self.fields['arg0'] is not None else " -----"
        rpt_str += f" {self.fields['arg1_res']:<5}" if self.fields['arg1_res'] is not None else " -----"
        #----
        if result == 'printing':
            print(rpt_str)
        else:
            return(rpt_str)

#----
class RARStMonitor:
    """A RAR status monitor"""
    def __init__(self, inst_name: str, port: dict, pnum: int):
        self.rpt = []
        self.is_err = False
        self.port = {}
        self.inst_name = inst_name
        self.port['rst_n'] = port['rst_n']
        self.port['clk'] = port['clk']
        self.port['o_stop_decode'] = port['o_stop_decode']
        self.port['i_du_str_rdy'] = port['i_du_str_rdy']
        self.port['i_du_str'] = port['i_du_str']
        self.port['o_tst_rar_str_st'] = port['o_tst_rar_str_st']
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'cocotb.{type(self).__qualname__}({self.inst_name})')
    #----
    def __str__(self):
        return self.inst_name + f'({type(self).__qualname__})'
    #----
    async def output_capture(self):
        """The main RAR status monitor routine"""
        ref_list_indx = 0
        while True:
            await RisingEdge(self.port["clk"])
            self.log.debug("\nRAR monitor senses a clock rising edge")
            if self.port["rst_n"].value.binstr == '1':
                self.log.debug("\nRAR monitor is in active mode")
                fl_du_str_rdy = 0
                for item in self.port["i_du_str_rdy"]:
                    if item.value.binstr == '1':
                        fl_du_str_rdy += 1
                if fl_du_str_rdy > 0 \
                    and self.port["o_stop_decode"].value.binstr == '0':
                    #----
                    rpt = f"\t#{ref_list_indx:<6} "
                    rpt += self.compose_state(self.port["o_tst_rar_str_st"])
                    ref_list_indx += 1
                    self.rpt.append(rpt)
    #----
    async def start(self):
        cocotb.start_soon(self.output_capture())
    #----
    def compose_state(self, str_arr):
        """
        Compose a status report for the RAR
        """
        output = ""
        for item in str_arr:
            output += f"{code_gen.IMPLICA_RAR_STR_STATES(item.value).name:<5} "
        #----
        return output
    #----
    def print_summary(self, mode: int):
        """
        Prints results of comparison
            0 - a full report (only errors)
            1 - a full report (all)
            2 - a recent comparison
        """
        print("\n" + self.__str__() + " report **********:")
        #----
        if mode == 2:
            print(self.rpt[-1])
        else:
            for item in self.rpt:
                if mode == 0 and 'FAIL' in item:
                    print(item)
                elif mode == 1:
                    print(item)
            if self.is_err == False:
                print("There are no RAR failures in the test\n")
        print("")
