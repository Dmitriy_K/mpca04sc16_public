#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
from cocotb.log import SimLog
from cocotb.triggers import RisingEdge
from cocotb.utils import lazy_property
from cocotb.binary import BinaryValue
from cocotb.handle import ModifiableObject

#----
class SRAMModel:
    """An SRAM model"""
    def __init__(self, inst_name: str, port: dict, width: int = 8, size: int = 128):
        """
        Set width in bits and size in words
        Port is a set of memory inputs/outputs represented in the dictionary type

        Possible connections:
        rstn - '1', '0', dut.port
        clk  - dut.port
        cen  - '1', '0', dut.port
        oen  - '1', '0', dut.port
        wen  - 'RO', 'WO', dut.port
        addr - dut.port
        di   - None, dut.port
        do   - None, dut.port
        """
        # for item in port:
        #     print(f'{port[item]}: {type(port[item])}')
        self.port = {}
        self.inst_name = inst_name
        self.x_val = 'X' * width
        self.width = width
        self.size = size
        self.bytes_in_word = int(width / 8)
        self.mem = [BinaryValue(self.x_val, width)] * size
        self.port["rstn"] = port["rstn"]
        self.port["clk"] = port["clk"]
        self.port["cen"] = port["cen"]
        self.port["oen"] = port["oen"]
        self.port["wen"] = port["wen"]
        self.port["addr"] = port["addr"]
        self.port["di"] = port["di"]
        self.port["do"] = port["do"]
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'cocotb.{type(self).__qualname__}({self.inst_name})')
    #----
    def __str__(self):
        return self.inst_name + f'({type(self).__qualname__}({self.width}x{self.size}))'
    #----
    async def start(self):
        """The main SRAM routine"""
        RSTN_INDX = 0
        CEN_INDX = 1
        WEN_INDX = 2
        ADDR_INDX = 3
        DI_INDX = 4
        while True:
            await RisingEdge(self.port["clk"])
            self.log.debug("\tMemory senses a clock edge")
            trn = self.b2trn()
            if trn[RSTN_INDX].binstr == '1':
                self.log.debug("\tMemory is in a working mode")
                if not trn[CEN_INDX].is_resolvable:
                    self.log.warning("\t'cen'(%s) is invalid" % trn[CEN_INDX].binstr)
                elif not trn[ADDR_INDX].is_resolvable:
                    self.log.warning("\tAddress \'%s\' is invalid" % trn[ADDR_INDX].binstr)
                elif not trn[WEN_INDX].is_resolvable:
                    self.log.warning("\t'wen'(%s) is invalid" % trn[WEN_INDX].binstr)
                else:
                    if trn[CEN_INDX].binstr == '0':
                        full_addr = trn[ADDR_INDX].integer
                        row_address = int(trn[ADDR_INDX].integer / self.bytes_in_word)
                        if row_address < self.size:
                            self.log.debug("\tMemory got a transaction with a valid dddress \'%s\'" % trn[ADDR_INDX].binstr)
                            if trn[WEN_INDX] == BinaryValue('1'*self.bytes_in_word, self.bytes_in_word):
                                self.log.debug("\tRD: address - %x(%x), data - %s" % (full_addr, row_address, self.mem[row_address]))
                                self.port["do"].value = self.mem[row_address] if self.port["do"] is not None else None
                            else:
                                self.log.debug("\tWR: address - %x(%x), data - %x" % (full_addr, row_address, trn[DI_INDX].integer))
                                self.mem[row_address] = trn[DI_INDX].binstr
                        else:
                            self.log.warning("\tAddress 0x%x is out of range. Memory size is %d" % (trn[ADDR_INDX].integer, self.size))
            else:
                self.log.debug("\tMemory is under reset")
    #----
    def load(self, image: list):
        """
        Loads memory with provided values
        The provided image should starts with 0 address and be non-zero length list
        """
        image_len = len(image)
        if image_len > 0 and image_len <= self.size:
            for i in range(image_len):
                self.mem[i] = image[i] if isinstance(image[i], BinaryValue) else BinaryValue(image[i], self.width)
        else:
            self.log.error("\tThe provided image(%d) is empty or exceeds the memory size(%d)" % len(image), self.size)
    #----
    def print_dump(self):
        """Prints the memory dump"""
        addr = 0
        for item in self.mem:
            print(f'{addr:>{self.width}x} : {item}')
            addr += self.bytes_in_word
    #----
    def b2trn(self) -> tuple:
        """converts input signals to an internal transaction"""
        rstn = BinaryValue(self.port["rstn"], 1) if type(self.port["rstn"]) in (str, int) \
                                                    else self.port["rstn"].value

        cen = BinaryValue(self.port["cen"], 1) if type(self.port["cen"]) in (str, int) \
                                                    else self.port["cen"].value
        if isinstance(self.port["wen"], str):
            if self.port["wen"] == 'RO':
                wen = BinaryValue('1'*self.bytes_in_word, self.bytes_in_word)
            elif self.port["wen"] == 'WO':
                wen = BinaryValue('0'*self.bytes_in_word, self.bytes_in_word)
            else:
                self.log.warning("\t'wen' is assigned to an invalid value: %s" % self.port["wen"])
        else:
            wen = self.port["wen"].value
        addr = self.port["addr"].value
        di = self.port["di"].value if self.port["di"] is not None \
                                    else BinaryValue(self.x_val, self.width)
        #----
        return (rstn, cen, wen, addr, di)
