#-------------------------------------------------------------------------------
#
# *  *   *  **   *    *   **   **
#    ** **  * *  *       *  *  * *
# *  * * *  * *  *    *  *     *  *
# *  * * *  * *  *    *  *     *  *
# *  * * *  **   * *  *  *  *  ****
# *  *   *  *    ***  *   **   *  *
#------------ Urals the ridge
# mpca04sc16
#-------------------------------------------------------------------------------
import cocotb
from cocotb.log import SimLog
from cocotb.triggers import RisingEdge
from cocotb.binary import BinaryValue
from cocotb.utils import lazy_property
import code_gen
from rar_monitor import RARstr

#----
BUF_SIZE = 16

#----
class DUResMonitor:
    """An DU output monitor"""
    def __init__(self, inst_name: str, port: dict, pnum: int):
        self.pnum = pnum
        self.pre_tag = 0
        self.cur_tag = 0
        self.rpt = []
        self.is_err = False
        self.cmd_stream = []
        self.port = {}
        self.inst_name = inst_name
        self.port["rst_n"] = port["rst_n"]
        self.port["clk"] = port["clk"]
        self.port["i_stop_decode"] = port["i_stop_decode"]
        self.port["i_fu_bp"] = port["i_fu_bp"]
        self.port["i_tag"] = port["i_tag"]
        self.port["i_mem"] = port["i_mem"]
        self.port["o_rar_str_rdy"] = port["o_rar_str_rdy"]
        self.port["o_rar_str"] = port["o_rar_str"]
    #----
    @lazy_property
    def log(self):
        """returns the component name and type for logging"""
        return SimLog(f'cocotb.{type(self).__qualname__}({self.inst_name})')
    #----
    def __str__(self):
        return self.inst_name + f'({type(self).__qualname__})'
    #----
    async def tag_capture(self):
        while True:
            await RisingEdge(self.port["clk"])
            if self.port["rst_n"].value.binstr == '1':
                self.cur_tag = self.pre_tag
                self.pre_tag = (self.port["i_tag"].value + self.pnum) % BUF_SIZE
    #----
    async def res_capture(self):
        """The main DU monitor routine"""
        ref_list_indx = 0
        while True:
            await RisingEdge(self.port["clk"])
            self.log.debug("\tDU monitor senses a clock rising edge")
            if self.port["rst_n"].value.binstr == '1':
                self.log.debug("\tDU monitor is in active mode")
                if self.port["o_rar_str_rdy"].value.binstr == '1' \
                    and self.port["i_stop_decode"].value.binstr == '0':
                    self.log.debug("\tDU got a new string")
                    curr_str = self.parse_str(self.port["o_rar_str"])
                    ref_str = self.decode_cmd(self.cmd_stream[ref_list_indx])
                    #----
                    if len(self.cmd_stream) > 0 and ref_list_indx < len(self.cmd_stream):
                        rpt = curr_str.print_str('string') + f"\t: (c) #{ref_list_indx:<6} "
                        if self.str_compare(curr_str, ref_str):
                            rpt += " OK"
                        else:
                            rpt += " FAIL"
                            self.is_err = True
                        rpt += "\n" + ref_str.print_str('string') + f"\t: (r)"
                        ref_list_indx += 1
                        self.rpt.append(rpt)
                    else:
                        self.log.error("\tNo reference value was provided")
    #----
    async def start(self):
        cocotb.start_soon(self.tag_capture())
        cocotb.start_soon(self.res_capture())
    #----
    def set_stream_to_compare(self, stream: list):
        """
        sets reference value(s) to compare
        items should have the IMPLICACommand type
        """
        if isinstance(stream, list):
            self.cmd_stream = stream
            self.log.info("\tA command stream length of %d is loaded", len(self.cmd_stream))
        else:
            self.log.error("\tProvided variable is not a 'list' type but a '%s' type", type(stream))
    #----
    def parse_str(self, str: BinaryValue) -> RARstr:
        """
        Parses data from DU string output to the RARstr type output
        """
        fields = {
            'st':code_gen.IMPLICA_RAR_STR_STATES(str.st.value).name,
            'tag': str.tag.value,
            'exn': code_gen.IMPLICA_EXE_TYPES(str.exn.value).name,
            'op': code_gen.IMPLICACommand.get_op_abbreviation(str.op.value, None, code_gen.IMPLICA_EXE_TYPES(str.exn.value).name),
            'eol': str.eol.value.integer,
            'bp': str.bp.value,
            'ss_arg0': code_gen.IMPLICA_SRCS(str.ss_arg0.value).name,
            'ss_arg1': code_gen.IMPLICA_SRCS(str.ss_arg1.value).name,
            'res_fl': code_gen.IMPLICA_CND_STATES(str.res_fl.value).name,
            'arg0': str.arg0.value,
            'arg1_res': str.arg1_res.value
        }
        res = RARstr(fields)
        #----
        return res
    #----
    def decode_cmd(self, cmd: code_gen.IMPLICACommand) -> RARstr:
        """
        Decodes a command with the IMPLICACommand type to the RARstr
        """
        fields = {}
        #----
        if cmd.fields['ss_arg0'] == code_gen.IMPLICA_SRCS.NA.name \
            and ((cmd.fields['ss_ref_base_arg1'] == code_gen.IMPLICA_SRCS.MEM.name and cmd.fields['am_arg1'] == code_gen.IMPLICA_ADDR_MODES.DISP.name) \
                or cmd.fields['am_arg1'] == code_gen.IMPLICA_ADDR_MODES.DIR.name):
            fields['st'] = code_gen.IMPLICA_RAR_STR_STATES.SET.name
        else:
            fields['st'] = code_gen.IMPLICA_RAR_STR_STATES.WAIT.name
        #----
        fields['tag'] = self.cur_tag
        #----
        if code_gen.IMPLICACommand.get_op_code(cmd.fields['operation']) >= 0b11001 or cmd.fields['format'] == 2:
            fields['exn'] = code_gen.IMPLICA_EXE_TYPES.CU.name
        elif cmd.fields['operation'] == 'MULH' or cmd.fields['operation'] == 'MULL':
            fields['exn'] = code_gen.IMPLICA_EXE_TYPES.MUL.name
        elif code_gen.IMPLICACommand.get_op_code(cmd.fields['operation']) <= 0b00101:
            fields['exn'] = code_gen.IMPLICA_EXE_TYPES.MAC.name
        else:
            fields['exn'] = code_gen.IMPLICA_EXE_TYPES.ALU.name
        #----
        fields['op'] = cmd.fields['operation']
        #----
        fields['eol'] = cmd.fields['eol']
        #----
        fields['bp'] = self.port["i_fu_bp"].value
        #----
        fields['ss_arg0']  = cmd.fields['ss_arg0']
        #----
        if cmd.fields['am_arg1'] == code_gen.IMPLICA_ADDR_MODES.DIR.name:
            if cmd.fields['ss_arg1'] == code_gen.IMPLICA_SRCS.CG.name:
                fields['arg1_res'] = cmd.fields['field1']
                fields['ss_arg1'] = code_gen.IMPLICA_SRCS.NA.name
            elif cmd.fields['ss_arg1'] == code_gen.IMPLICA_SRCS.RAR.name:
                fields['arg1_res'] = ((fields['tag'] + BUF_SIZE) - cmd.fields['field1']) % BUF_SIZE
                fields['ss_arg1'] = code_gen.IMPLICA_SRCS.RAR.name
        elif cmd.fields['am_arg1'] == code_gen.IMPLICA_ADDR_MODES.DISP.name:
            if cmd.fields['ss_ref_base_arg1'] == code_gen.IMPLICA_SRCS.MEM.name:
                fields['arg1_res'] = self.port["i_mem"][cmd.fields['field0']].value + cmd.fields['field1']
                fields['ss_arg1'] = code_gen.IMPLICA_SRCS.NA.name
            elif cmd.fields['ss_ref_base_arg1'] == code_gen.IMPLICA_SRCS.RAR.name:
                fields['arg1_res'] = cmd.fields['field1']
                fields['ss_arg1'] = code_gen.IMPLICA_SRCS.CALC.name
            else:
                self.log.error(f"\tss_ref_base_arg1 = {cmd.fields['ss_ref_base_arg1']} is not available if am_arg1 = DISP")
        else:
            self.log.error(f"\tam_arg1 = {cmd.fields['am_arg1']} is not available")
        #----
        fields['res_fl'] = code_gen.IMPLICA_CND_STATES.NA.name
        #----
        if cmd.fields['am_arg0'] != code_gen.IMPLICA_ADDR_MODES.NA.name:
            fields['arg0'] = ((fields['tag'] + BUF_SIZE) - cmd.fields['field0']) % BUF_SIZE
        else:
            fields['arg0'] = 0
        #----
        res = RARstr(fields)
        #----
        return res
    #----
    def str_compare(self, curr_str: RARstr, ref_str: RARstr):
        """
        compares a DUT out sample and a reference value
        """
        if  curr_str.fields['st']           == ref_str.fields['st'] \
            and curr_str.fields['tag']      == ref_str.fields['tag'] \
            and curr_str.fields['exn']      == ref_str.fields['exn'] \
            and curr_str.fields['op']       == ref_str.fields['op'] \
            and curr_str.fields['eol']      == ref_str.fields['eol'] \
            and curr_str.fields['bp']       == ref_str.fields['bp'] \
            and curr_str.fields['ss_arg0']  == ref_str.fields['ss_arg0'] \
            and curr_str.fields['ss_arg1']  == ref_str.fields['ss_arg1'] \
            and curr_str.fields['res_fl']      == ref_str.fields['res_fl'] \
            and curr_str.fields['arg0']     == ref_str.fields['arg0'] \
            and curr_str.fields['arg1_res'] == ref_str.fields['arg1_res']:
            return 1
        else:
            return 0
    #----
    def print_summary(self, mode: int):
        """
        Prints results of comparison
            0 - a full report (only errors)
            1 - a full report (all)
            2 - a recent comparison
        """
        print("\n" + self.__str__() + " report **********:")
        RARstr.print_header('printing')
        #----
        if mode == 2:
            print(self.rpt[-1])
        else:
            for item in self.rpt:
                if mode == 0 and 'FAIL' in item:
                    print(item)
                elif mode == 1:
                    print(item)
            if self.is_err == False:
                print("There are no DU failures in the test\n")
        print("")
