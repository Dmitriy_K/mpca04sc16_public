# An mpca04sc16 CPU repository

You can see an mpca04sc16 architecture explanation [here](https://mpca-cpu.tech/doku.php?id=start).

The TB of the 'mpca04sc16' project is based on CocoTB. See information about the CocoTB module [here](https://docs.cocotb.org/en/stable/index.html).

**Used languages and data types:**
|Language/type |Usage
|:----         |:----
|DrawIO        |documentation (block schemes)
|JSON          |documentation (wave diagrams, WaveDrom)
|SystemVerilog |design
|Python        |project environment management and TB(CocoTB)

# The repository structure
## The directories tree

```
<prj_dir>
    ├─ design                        - design source code
    ├─ docs                          - documentation related to the project
    ├─ *run                          - a directory to run tools
    │   └─ <work directory>          - a directory for the certain test/unit/case
    │       ├─ design                - a link to the design directory
    │       ├─ <unit name>           - a link to the unit tests directory
    │       └─ rpt                   - a report folder
    │           └─ <YYYYMMDD_HHMMSS> - a certain run report folder
    └── ver                          - verification settings, tools and tests
        ├─ common                    - common components for units TBs
        ├─ <unit name>               - a directory with unit TBs ('top' is a reserved name for the top    unit)
        │   └─ cfg                   - a directory with unit TB settings
        └─ run_tool                  - a run tool directory
            ├─ classes
            └─ modules

    *run - the directory is not a part of the repository, it is temporary and being created locally
```
## Files
| Directory                      | File           | Description |
| :----                          | :----          | :----
| .                              | `README.md`    | The file you are reading now, a project environment user manual
| .                              | `LICENSE`      | The license file
| .                              | `project.yaml` | Project settings
| .                              | `setup.sh`     | A script to setup environment variables
| `./design`                     |                |
|                                | `*.sv, *.svh`  | Design source files
| `./run`                        |                |
| `./run/<work directory>`       |                |
|                                | `*.py`         | (temporary) link(s) to tests
|                                | `project.yaml` | parsed project settings
|                                | `results.xml`  | A CocoTB result report
|                                | `transcript`   | A CocoTB test log
|                                | `*.*`          | different files which presence depends on used tools
| `./run/<work directory>/<rpt>` |                |
|                                | `*.log`        | Recent run 0.-tools reports
| `./ver/common`                 |                |
|                                | `*.py`         | Common TB components
| `./ver/<unit name>`            |                |
|                                | `*_tst.py`     | A test to verify the corresponding unit
| `./ver/<unit name>/cfg`        |                |
|                                | `cad_settings_cov.f`         | coverage settings
|                                | `cad_settings_design_cmpl.f` | design compilation options
|                                | `cad_settings_elab.f`        | elaboration options
|                                | `cad_settings_sim.f`         | simulation options
|                                | `src_list_design.f`          | a design source list
| `./ver/run_tool`               |          |
|                                | `run.py` | The main script file
| `./ver/run_tool/classes`       |          |
|                                | `*.py`   | Run tool components (classes)
| `./ver/run_tool/modules`       |          |
|                                | `*.py`   | Run tool components (modules)

# Quick run
1. Check which a simulator you have installed. See information about suppported platforms on the [CocoTB site](https://docs.cocotb.org/en/stable/simulator_support.html)
1. Be sure you have installed CocoTB module. Follow the instructions on the [CocoTB site](https://docs.cocotb.org/en/stable/install.html)
1. Move to the `<prj_dir>` folder
1. Execute the next commnad ```source setup.sh``` (do it every time you run a new terminal/console)
1. Create a work directory with the next command: ```run.py -cwd unit=<unit name> dir=<work directory>```, for example: ```run.py -cwd unit=top dir=top_tst_1```
1. Move to the created work directory
1. Run design compilation: ```run.py -cdesign```
1. Run simulation: ```run.py -sim tn=<test_name>```, for example: ```run.py -sim tn=mix_frmt_wo_dly_tst```

# Typical design flow

# Typical verification flow
