//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module rar (
    input  wire             i_rst_n,
    input  wire             i_clk,
    output reg              o_stop_decode,  // rar is full
    //---- du interconnect
    input  wire             i_du_str_rdy [NOC],
    input  rar_str_struct   i_du_str [NOC],
    //---- exe interconnect
    output reg              o_exe_rqst [NOE],
    output exe_task_struct  o_exe_task [NOE],
    input  wire             i_exe_res_rdy [NOE],
    input  exe_res_struct   i_exe_res [NOE],
    //---- tst ports
    output rar_str_st_t     o_tst_rar_str_st [BUF_SIZE]
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    wire arr_muxed_res_rdy [BUF_SIZE];
    exe_res_struct arr_muxed_res [BUF_SIZE];
    rar_str_struct arr_str [BUF_SIZE];

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
    rar_buf buffer (
        .i_rst_n        (i_rst_n),
        .i_clk          (i_clk),
        .o_full         (o_stop_decode),
        .i_new_str_rdy  (i_du_str_rdy),
        .i_new_str      (i_du_str),
        .i_arr_res_rdy  (arr_muxed_res_rdy),
        .i_arr_res      (arr_muxed_res),
        .o_arr_str      (arr_str)
    );
    //----
    rar_nba_mux #(
        .CH_T   (exe_res_struct),
        .NOIC   (NOE),
        .NOOC   (BUF_SIZE)
    )
    res_mux (
        .i_arr_ch_rdy   (i_exe_res_rdy),
        .i_arr_ch       (i_exe_res),
        .o_arr_ch_rdy   (arr_muxed_res_rdy),
        .o_arr_ch       (arr_muxed_res),
        .o_err          ()
    );
    //----
    generate
        for (genvar gen_exu_indx = 0; gen_exu_indx < NOE; gen_exu_indx++) begin : exe_mux_gen
            rar_exe_mux #(
                .EXE_TYPE       (exe_t'(gen_exu_indx))
            )
            exe_mux (
                .i_rst_n        (i_rst_n),
                .i_clk          (i_clk),
                .i_arr_str      (arr_str),
                .i_exe_res_rdy  (i_exe_res_rdy[gen_exu_indx]),
                .o_exe_rqst     (o_exe_rqst[gen_exu_indx]),
                .o_exe_task     (o_exe_task[gen_exu_indx])
            );
        end
    endgenerate
    //----
    generate
        for (genvar gen_str_indx = 0; gen_str_indx < BUF_SIZE; gen_str_indx++) begin : tst_port_tag_gen
            assign  o_tst_rar_str_st[gen_str_indx] = arr_str[gen_str_indx].st;
        end
    endgenerate

endmodule
