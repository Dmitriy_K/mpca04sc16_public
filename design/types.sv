//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
// general parameters and data types
//------------------------------------------------------------------------------
typedef logic [WEN_W-1:0] wen_t;
typedef integer unsigned tag_t;
typedef integer unsigned addr_t;
typedef integer unsigned data_t;
typedef integer unsigned operation_t;
typedef integer unsigned brake_pnt_t;
//----
typedef enum integer {ALU = 0, MAC = 1, CU = 2, MUL = 3, NA_EXE} exe_t;
typedef enum integer {CF, OVD, NZ, Z, NCEF, CEF, NA_CND} cnd_st_t;
typedef enum integer {RAR, CG, MEM, CALC, NA_SRC} ss_t;
typedef enum integer {DIR, IND, DISP, NA_AM} am_t;
typedef enum integer {EMPTY, WAIT, SET, RES} rar_str_st_t;
//----
// args in rhe rar may wait for results from exe
// ss_cnd, ss_arg0, ss_arg1 - show what a source respective to arg is waiting for
//----
typedef struct packed{
    rar_str_st_t    st;
    tag_t           tag;
    exe_t           exn;
    operation_t     op;
    logic           eol;
    brake_pnt_t     bp;
    ss_t            ss_arg0;   // rar or none
    ss_t            ss_arg1;   // rar or calc or none
    cnd_st_t        res_fl;    // res flags
    data_t          arg0;      // f0, arg0
    data_t          arg1_res;  // f1, arg1, res
} rar_str_struct;
//----
typedef struct packed{
  tag_t   tag;
  operation_t    op;
  logic                 eol;
  brake_pnt_t    bp;
  data_t  arg0;
  data_t  arg1;
} exe_task_struct;
//----
typedef struct packed{
  tag_t   tag;
  cnd_st_t fl;
  data_t  res;
} exe_res_struct;
//----
typedef struct packed{
    logic   rdy;
    data_t  data;
} dma_ch_reply_struct;
//----
typedef struct packed{
    logic   rqst;
    wen_t   we;
    addr_t  addr;
    data_t  data;
} dma_ch_rqst_struct;
//----
typedef struct packed {
    brake_pnt_t bp;
    data_t      data;
} instr_ch_data_struct;
//---- basic cu operations
typedef enum operation_t {
    NOP     = operation_t'('b11001),
    RR      = operation_t'('b11010), //GF
    SYS     = operation_t'('b11011), //stop, rst
    EOL     = operation_t'('b11100),
    EXA     = operation_t'('b11101),
    CALL    = operation_t'('b11110),
    RET     = operation_t'('b11111),
    LDC     = operation_t'('b00000)
} op_cu_t;
//---- basic alu operations
typedef enum operation_t {
    ADD     = operation_t'('b01000),
    ADDC    = operation_t'('b01001),
    SUB     = operation_t'('b01010),
    SUBB    = operation_t'('b01011),
    BC      = operation_t'('b01100),
    BSM     = operation_t'('b01101),
    BSL     = operation_t'('b01110),
    CSL     = operation_t'('b01111),
    CSR     = operation_t'('b10000),
    LSL     = operation_t'('b10001),
    LSR     = operation_t'('b10010),
    ASL     = operation_t'('b10011),
    ASR     = operation_t'('b10100),
    AND     = operation_t'('b10101),
    OR      = operation_t'('b10110),
    XOR     = operation_t'('b10111),
    NOT     = operation_t'('b11000)
} op_alu_t;
//---- basic mul operations
typedef enum operation_t {
    MULL    = operation_t'('b00110),
    MULH    = operation_t'('b00111)
} op_mul_t;
//---- basic mac operations
typedef enum operation_t {
    LDUB    = operation_t'('b00000), //POPB
    LDSB    = operation_t'('b00001),
    LDU2B   = operation_t'('b00010), //POP2B
    LDS2B   = operation_t'('b00011),
    STB     = operation_t'('b00100), //PUSHB
    ST2B    = operation_t'('b00101)  //PUSH2B
} op_mac_t;
