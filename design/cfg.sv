//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import prj_func::bytes_in_word;
//----
parameter int TAG_W         = 'd4;
parameter int NARGS         = 'd3;      // max number of args
parameter int NOC           = 'd1;      // number of cells in cpu
parameter int ADDR_W        = 'd16;     // main memory address width
parameter int DATA_W        = 'd16;     // instruction word width
parameter int BP_W          = 'd2;      // brake point
parameter int INT_W         = 'd16;     // interrupt bus width
parameter int BUF_SIZE      = 2**TAG_W;
parameter int BIDW          = bytes_in_word (DATA_W);
parameter int WEN_W         = BIDW;
parameter int RAR_TYPE      = 1;        // 1 - sequential cyclical tagging, 2 - first free position is a tag
parameter int INT_BUS_NOSUB = 'd1;      // number of subscribers of the internal data bus (inside cpu)
parameter int INT_BUS_NOSRC = 'd2;      // number of sources of the internal data bus (inside cpu)
//---- command word fields
parameter int F_POS_E           = 'd1;
parameter int F_POS_B           = 'd0;
parameter int SS_ARG1_POS       = 'd15;
parameter int AM_SS_ARG1_POS_E  = 'd14;
parameter int AM_SS_ARG1_POS_B  = 'd13;
parameter int EOLCS_POS         = 'd15;
parameter int OP_FRMT0_POS_E    = 'd14;
parameter int OP_FRMT0_POS_B    = 'd10;
parameter int OP_FRMT1_POS_E    = 'd12;
parameter int OP_FRMT1_POS_B    = 'd10;
parameter int F0_FRMT0_POS_E    = 'd9;
parameter int F0_FRMT0_POS_B    = 'd6;
parameter int F1_FRMT0_POS_E    = 'd5;
parameter int F1_FRMT0_POS_B    = 'd2;
parameter int F0_FRMT1_POS_E    = 'd9;
parameter int F0_FRMT1_POS_B    = 'd6;
parameter int F1_FRMT1_POS_E    = 'd5;
parameter int F1_FRMT1_POS_B    = 'd2;
parameter int F1_FRMT2_POS_E    = 'd15;
parameter int F1_FRMT2_POS_B    = 'd2;
//----
parameter int OP_W              = 'd5;
parameter int F_W               = 'd2;
parameter int NOE               = 4;