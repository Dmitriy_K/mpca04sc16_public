//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module cpu (
    input   wire                i_rst_n,
    input   wire                i_clk,
    output  wire                o_rst_rqst,
    //---- external interrupts
    input   wire [INT_W-1:0]    i_ext_int,
    //---- instruction dma
    output  wire                o_i_dma_cen_ch0,
    input   wire                i_i_dma_rdy_ch0,
    output  wire [ADDR_W-1:0]   o_i_dma_addr_ch0,
    input   wire [BP_W-1:0]     i_i_dma_bp_ch0,
    input   wire [DATA_W-1:0]   i_i_dma_data_ch0,
    //---- data dma
    input   wire                i_d_dma_rdy_ch0,
    output  wire                o_d_dma_cen_ch0,
    output  wire [WEN_W-1:0]    o_d_dma_wen_ch0,
    output  wire [ADDR_W-1:0]   o_d_dma_addr_ch0,
    input   wire [DATA_W-1:0]   i_d_dma_data_ch0,
    output  wire [DATA_W-1:0]   o_d_dma_data_ch0,
    //---- tst i/f
    input   wire                i_tm,
    input   wire                i_tst_stop_fetch
);
//------------------------------------------------------------------------------
//---- constants
//------------------------------------------------------------------------------
    localparam integer SRC_SIZE_ARR [INT_BUS_NOSRC] = '{MEM_MM_BLK_SIZE, MEM_SR_BLK_SIZE};
    localparam addr_t SRC_B_ADDR_ARR [INT_BUS_NOSRC] = '{MEM_MM_B_ADDR, MEM_SR_B_ADDR};
//------------------------------------------------------------------------------
//---- variables
//------------------------------------------------------------------------------
    wire w_sw_rst;
    integer w_l_num;
    wire w_halt;
    wire w_int_rqst;
    wire w_generic_eolcs;
    wire w_eolcs [NOC];
    wire w_generic_fu_instr_rdy;
    wire w_generic_stop_fetch;
    wire w_du_stop_fetch [NOC];
    wire w_stop_decode;
    wire w_cu_addr_rdy;
    addr_t w_cu_next_addr;
    integer v_tag;
    data_t w_mem_sr [MEM_NOSYSR];
    //----
    wire        w_fu2du_rdy [NOC];
    integer     w_fu2du_frmt [NOC];
    wire        w_fu2du_eolcs [NOC];
    brake_pnt_t w_fu2du_bp [NOC];
    data_t      w_fu2du_instr [NOC];
    wire            w_du2rar_str_rdy [NOC];
    rar_str_struct  w_du2rar_str [NOC];
    //----
    dma_ch_rqst_struct  w_src_rqst [INT_BUS_NOSRC];
    dma_ch_reply_struct w_src_reply [INT_BUS_NOSRC];
    dma_ch_rqst_struct  w_sub_rqst [INT_BUS_NOSUB];
    dma_ch_reply_struct w_sub_reply [INT_BUS_NOSUB];
    //----
    wire            w_exe_res_rdy [NOE];
    wire            w_exe_rqst [NOE];
    exe_res_struct  w_exe_res [NOE];
    exe_task_struct w_exe_task [NOE];
    //----
    addr_t         w_i_dma_addr_ch0;
    brake_pnt_t    w_i_dma_bp_ch0;
    data_t         w_i_dma_data_ch0;

    //------------------------------------------------------------------------------
    //---- logic
    //------------------------------------------------------------------------------
    assign w_sw_rst = '0;
    assign w_l_num = '0;
    assign w_halt = '0;
    assign w_int_rqst = | i_ext_int;
    assign w_generic_eolcs = w_eolcs.or;
    assign w_generic_fu_instr_rdy = w_fu2du_rdy.or;
    assign w_generic_stop_fetch = (i_tm) ? w_du_stop_fetch.or : i_tst_stop_fetch;

    assign o_i_dma_addr_ch0 = (ADDR_W)'(w_i_dma_addr_ch0);
    assign w_i_dma_bp_ch0   = brake_pnt_t'(i_i_dma_bp_ch0);
    assign w_i_dma_data_ch0 = data_t'(i_i_dma_data_ch0);

    // a gag for absent components
    logic v_rst_rqst;
    logic v_stop_fetch;
    logic v_d_dma_cen_ch0;
    logic v_d_dma_wen_ch0;
    logic [ADDR_W-1:0] v_d_dma_addr_ch0;
    logic [DATA_W-1:0] v_d_dma_data_ch0;
    assign w_cu_addr_rdy = '0;
    assign w_cu_next_addr = '0;
    assign w_exe_res_rdy = '{default: '0};
    assign w_exe_res = '{default: '0};

    assign w_src_reply[SRC_CN_MM].rdy = '0;
    assign w_sub_rqst[SUB_CN_MAC].rqst = '0;
    assign w_sub_rqst[SUB_CN_MAC].we = '0;
    assign w_sub_rqst[SUB_CN_MAC].addr = '0;
    assign w_sub_rqst[SUB_CN_MAC].data = '0;

    assign o_rst_rqst       = v_rst_rqst;
    assign o_d_dma_cen_ch0  = v_d_dma_cen_ch0;
    assign o_d_dma_wen_ch0  = v_d_dma_wen_ch0;
    assign o_d_dma_addr_ch0 = v_d_dma_addr_ch0;
    assign o_d_dma_data_ch0 = v_d_dma_data_ch0;

    //----
    always_ff @(posedge(i_clk) or negedge(i_rst_n)) begin: dmem_drv
        if (!i_rst_n) begin
            v_d_dma_cen_ch0   <= '1;
            v_d_dma_wen_ch0   <= '1;
            v_d_dma_addr_ch0  <= 'x;
            v_d_dma_data_ch0  <= 'x;
        end
        else begin
            v_d_dma_cen_ch0   <= '0;
            v_d_dma_wen_ch0   <= '0;
            v_d_dma_addr_ch0  <= (v_d_dma_cen_ch0) ? '0 : v_d_dma_addr_ch0 + 'd2;
            v_d_dma_data_ch0  <= (v_d_dma_cen_ch0) ? '0 : v_d_dma_data_ch0 + 'd4;
        end
    end

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
    //---- fetch units
    fu fu0 (
        .i_rst_n        (i_rst_n),
        .i_clk          (i_clk),
        .i_l_num        (w_l_num),
        .i_halt         (w_halt),
        .i_sw_rst       (w_sw_rst),
        .i_stop_fetch   (w_generic_stop_fetch),
        .i_int_rqst     (w_int_rqst),
        .o_eolcs        (w_eolcs[0]),
        .i_eolcs        (w_generic_eolcs),
        .i_cu_addr_rdy  (w_cu_addr_rdy),
        .i_cu_next_addr (w_cu_next_addr),
        .o_dma_rqst_n   (o_i_dma_cen_ch0),
        .i_dma_rdy      (i_i_dma_rdy_ch0),
        .o_dma_addr     (w_i_dma_addr_ch0),
        .i_dma_bp       (w_i_dma_bp_ch0),
        .i_dma_data     (w_i_dma_data_ch0),
        .o_du_instr_rdy (w_fu2du_rdy[0]),
        .o_du_frmt      (w_fu2du_frmt[0]),
        .o_du_eolcs     (w_fu2du_eolcs[0]),
        .o_du_bp        (w_fu2du_bp[0]),
        .o_du_instr     (w_fu2du_instr[0])
    );
    //---- decoders
    du du0 (
        .i_rst_n        (i_rst_n),
        .i_clk          (i_clk),
        .i_stop_decode  (w_stop_decode),
        .o_stop_fetch   (w_du_stop_fetch[0]),
        .i_tag          (v_tag),
        .i_mem          (w_mem_sr),
        .i_fu_instr_rdy (w_fu2du_rdy[0]),
        .i_fu_frmt      (w_fu2du_frmt[0]),
        .i_fu_eolcs     (w_fu2du_eolcs[0]),
        .i_fu_bp        (w_fu2du_bp[0]),
        .i_fu_instr     (w_fu2du_instr[0]),
        .o_rar_str_rdy  (w_du2rar_str_rdy[0]),
        .o_rar_str      (w_du2rar_str[0])
    );
    //---- tag generator
    tag_gen tag_gen (
        .i_rst_n        (i_rst_n),
        .i_clk          (i_clk),
        .i_new_tag_rqst (w_generic_fu_instr_rdy),
        .o_tag          (v_tag)
    );
    //---- system registers (the system registers memory strata)
    assign w_src_reply[SRC_CN_SR].rdy = '1;
    mem_sr mem_sr (
        .i_rst_n    (i_rst_n),
        .i_clk      (i_clk),
        .i_rqst     (w_src_rqst[SRC_CN_SR].rqst),
        .i_we       (w_src_rqst[SRC_CN_SR].we),
        .i_addr     (w_src_rqst[SRC_CN_SR].addr),
        .i_data     (w_src_rqst[SRC_CN_SR].data),
        .o_data     (w_src_reply[SRC_CN_SR].data),
        .o_arr_sr   (w_mem_sr)
    );
    //---- mem switch
    mux #(
        .NOSUB          (INT_BUS_NOSUB),
        .NOSRC          (INT_BUS_NOSRC),
        .SRC_SIZE       (SRC_SIZE_ARR),
        .SRC_B_ADDR     (SRC_B_ADDR_ARR)
    )
    internal_mem_mux (
        .i_rst_n        (i_rst_n),
        .i_clk          (i_clk),
        .i_sub_rqst     (w_sub_rqst),
        .o_sub_reply    (w_sub_reply),
        .o_src_rqst     (w_src_rqst),
        .i_src_reply    (w_src_reply)
    );
    //---- results array
    rar rar (
        .i_rst_n          (i_rst_n),
        .i_clk            (i_clk),
        .o_stop_decode    (w_stop_decode),
        .i_du_str_rdy     (w_du2rar_str_rdy),
        .i_du_str         (w_du2rar_str),
        .o_exe_rqst       (w_exe_rqst),
        .o_exe_task       (w_exe_task),
        .i_exe_res_rdy    (w_exe_res_rdy),
        .i_exe_res        (w_exe_res),
        .o_tst_rar_str_st ()
    );

endmodule
