//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
// The main function of the unit is taking instruction words
// from memory and passing it to the decoding unit
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;

module fu (
    //---- sys
    input  wire         i_clk,
    input  wire         i_rst_n,
    input  integer      i_l_num,            //
    input  wire         i_halt,             // Stall signal for the core
    input  wire         i_sw_rst,           // Software reset
    input  wire         i_stop_fetch,       // A module after fu isn't ready
    input  wire         i_int_rqst,
    output logic        o_eolcs,            // current eolcs signal caught by this fu instance
    input  wire         i_eolcs,            // the eolcs signs from all fu
    //---- cu
    input  wire         i_cu_addr_rdy,      // Next address is ready
    input  addr_t       i_cu_next_addr,     // Next address to fetch
    //---- dma
    output logic        o_dma_rqst_n,       // Request for the next instruction word
    input  wire         i_dma_rdy,          // Requested instruction word is ready
    output addr_t       o_dma_addr,         // Address of the next instruction word
    input  brake_pnt_t  i_dma_bp,           // bp
    input  data_t       i_dma_data,         // Requested instruction word
    //---- decoder interconnect
    output logic        o_du_instr_rdy,     // Instruction word is ready
    output integer      o_du_frmt,          // instruction format
    output logic        o_du_eolcs,         // eols (end of a line code section)
    output brake_pnt_t  o_du_bp,            // bp
    output data_t       o_du_instr          // Raw instruction word
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------
    localparam instr_ch_data_struct DUMMY_MAC_DIN = '{default:0};

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    logic   w_dma_rqst;
    logic   v_instr_rdy;
    integer v_format;
    logic   v_eolcs;
    logic   v_flush;
    logic   v_en_addr_inc;
    addr_t  v_addr_l_inc;
    addr_t  v_next_addr, v_cur_dma_addr;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    assign o_dma_rqst_n = ~w_dma_rqst;
    assign o_eolcs = v_eolcs;
    assign o_du_instr_rdy = v_instr_rdy;

    //---- earlier eolcs and format decoders
    assign v_format = integer'(i_dma_data[F_POS_E:F_POS_B]);
    assign v_eolcs = (v_format == 1) ? i_dma_data[EOLCS_POS] : '0;
    //----
    always_ff @ (posedge i_clk) begin : frmt_reg
        o_du_frmt <= v_format;
    end
    always_ff @ (posedge i_clk) begin : eolcs_reg
        o_du_eolcs <= v_eolcs;
    end

    //---- address incrementer
    always_comb begin : flush_gen
        if (!i_rst_n)
            v_flush = '0;
        else if ((o_du_eolcs || i_int_rqst) && v_instr_rdy)
            v_flush = '1;
        else
            v_flush = '0;
    end

    //----
    always_comb begin : addr_incr_sel
        case (i_l_num)
            'd0: v_addr_l_inc = BIDW*0;
            'd1: v_addr_l_inc = BIDW*1;
            'd2: v_addr_l_inc = BIDW*2;
            'd3: v_addr_l_inc = BIDW*3;
            default: v_addr_l_inc = 'd0;
        endcase
    end
    //----
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : new_addr_store
        if (!i_rst_n)           v_next_addr <= START_ADDR;
        else if (i_cu_addr_rdy) v_next_addr <= i_cu_next_addr;
    end

    //----
    assign v_cur_dma_addr = (v_flush && i_rst_n) ? v_next_addr + v_addr_l_inc :
                                (!v_flush && i_rst_n && w_dma_rqst) ? o_dma_addr + BIDW :
                                    START_ADDR;

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
    mac_1ph #(
        .WEN_W      (WEN_W),
        .MODE       ('1),
        .ADDR_BUS_T (addr_t),
        .DATA_BUS_T (instr_ch_data_struct)
    )
    mac (
        .i_rst_n    (i_rst_n),
        .i_clk      (i_clk),
        .i_en       ('1),
        .i_hold     (i_stop_fetch),
        .i_flush    (v_flush),
        .i_rqst     ('1),
        .i_we       ('0),
        .o_rdy      (v_instr_rdy),
        .o_raw_rdy  (),
        .i_id       (integer'(v_cur_dma_addr)),
        .o_id       (),
        .i_addr_inc (v_cur_dma_addr),
        .i_data     (DUMMY_MAC_DIN),
        .o_data     ({o_du_bp, o_du_instr}),
        .o_mac_rqst (w_dma_rqst),
        .o_mac_we   (),
        .i_mac_rdy  (i_dma_rdy),
        .o_mac_addr (o_dma_addr),
        .i_mac_data ({i_dma_bp, i_dma_data}),
        .o_mac_data ()
    );

endmodule
