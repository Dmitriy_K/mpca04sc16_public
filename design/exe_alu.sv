//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------

//----
module exe_alu import prj_pkg::*; (
  input  wire                 i_rst_n,
  input  wire                 i_clk,
  //---- rar interconnect
  input  wire                 i_exe_task_rqst,
  input  exe_task_struct      i_exe_task,
  output logic                o_exe_res_rdy,
  output exe_res_struct       o_exe_res
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
logic [DATA_W-1:0]  w_arg0, w_arg1;
logic [DATA_W-1:0]  w_shifter_res, w_bc_res, w_bsm_res, w_bsl_res, w_and_res, w_or_res, w_xor_res, w_not_res;
logic [DATA_W:0]    w_adder_res;
cnd_st_t            w_cnd;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
  assign w_arg0     = i_exe_task.arg0;
  assign w_arg1     = (i_exe_task.op inside {ADD, ADDC})  ? i_exe_task.arg1 :
                      (i_exe_task.op inside {SUB, SUBB})  ? !i_exe_task.arg1 + 'd1 :
                      '0;
  assign fl_arg1_c  = (i_exe_task.op inside {ADD, SUB})   ? '0 :
                      (i_exe_task.op inside {ADDC, SUBB}) ? (i_exe_task.cnd == CF) :
                      '0;

  //----
  always_comb begin : adder
    if (i_exe_task_rqst && i_exe_task.op inside {ADD, ADDC, SUB, SUBB}) begin
      w_adder_res = w_arg0 + w_arg1 + fl_arg1_c;
    end
  end

  // fixme - cyclic shift isn't ready now
  always_comb begin : shifter
    if (i_exe_task_rqst) begin
      case (i_exe_task.op)
        CSL :     w_shifter_res = '0;
        CSR :     w_shifter_res = '0;
        LSL :     w_shifter_res = w_arg0 << w_arg1;
        LSR :     w_shifter_res = w_arg0 >> w_arg1;
        ASL :     w_shifter_res = DATA_W'(w_arg0 <<< w_arg1);
        ASR :     w_shifter_res = DATA_W'(w_arg0 >>> w_arg1);
        default : w_shifter_res = '0;
      endcase
    end
  end

  //----
  always_comb begin : bunch_of_logic_func
    if (i_exe_task_rqst) begin
      w_bc_res  = $countones(w_arg1);
      w_bsm_res = $left(w_arg1);
      w_bsl_res = $right(w_arg1);
      w_and_res = w_arg0 & w_arg1;
      w_or_res  = w_arg0 | w_arg1;
      w_xor_res = w_arg0 ^ w_arg1;
      w_not_res = ~w_arg1;
    end
  end

  //----
  always_comb begin : condition_selector
    if (w_adder_res[DATA_W])
      w_cnd = CF;
    else if (~|w_adder_res[DATA_W-1:0])
      w_cnd = Z;
    else if ((~w_arg0[DATA_W-1] &&  w_arg1[DATA_W-1] &&  w_adder_res[DATA_W-1]) ||
            (w_arg0[DATA_W-1] && ~w_arg1[DATA_W-1] && ~w_adder_res[DATA_W-1]))
      w_cnd = OVD;
    else
      w_cnd = NA_CND;
  end

  //----
  always_ff @ (posedge i_clk or negedge i_rst_n) begin : alu_operations
    if (!i_rst_n) begin
      o_exe_res_rdy <= '0;
    end
    else if (i_exe_task_rqst) begin
      o_exe_res.tag <= i_exe_task.tag;
      o_exe_res.cnd <= w_cnd;
      o_exe_res_rdy <= '1;

      case (i_exe_task.op)
        ADD,ADDC,SUB,SUBB:        o_exe_res.res <= w_adder_res;
        CSL,CSR,LSL,LSR,ASL,ASR:  o_exe_res.res <= w_shifter_res;
        BC  :                     o_exe_res.res <= w_bc_res;
        BSM :                     o_exe_res.res <= w_bsm_res;
        BSL :                     o_exe_res.res <= w_bsl_res;
        AND :                     o_exe_res.res <= w_and_res;
        OR  :                     o_exe_res.res <= w_or_res;
        XOR :                     o_exe_res.res <= w_xor_res;
        NOT :                     o_exe_res.res <= w_not_res;
        default:                  o_exe_res.res <= '0;
      endcase
    end
    else begin
      o_exe_res_rdy <= '0;
    end
  end


//------------------------------------------------------------------------------
//---- functions
//------------------------------------------------------------------------------
endmodule
