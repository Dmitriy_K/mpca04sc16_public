//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------

//----
module exe_mul import prj_pkg::*; (
  input  wire                 i_rst_n,
  input  wire                 i_clk,
  //---- rar interconnect
  input  wire                 i_exe_task_rqst,
  input  exe_task_struct      i_exe_task,
  output logic                o_exe_res_rdy,
  output exe_res_struct       o_exe_res
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
logic [DATA_W-1:0]    w_arg0, w_arg1;
logic [2*DATA_W-1:0]  w_res;
cnd_st_t              w_cnd;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
  assign w_arg0 = i_exe_task.arg0;
  assign w_arg1 = i_exe_task.arg1;

  //---- multiplier
  always_comb begin
    if (i_exe_task_rqst) w_res = w_arg0 * w_arg1;
  end

  //----
  always_comb begin : condition_selector
    if (w_res == 0) w_cnd = Z;
    else            w_cnd = NA_CND;
  end

  //----
  always_ff @ (posedge i_clk or negedge i_rst_n) begin : alu_operations
    if (!i_rst_n) begin
      o_exe_res_rdy <= '0;
    end
    else if (i_exe_task_rqst) begin
      o_exe_res.tag <= i_exe_task.tag;
      o_exe_res.cnd <= w_cnd;
      o_exe_res_rdy <= '1;

      case (i_exe_task.op)
        MULL:     o_exe_res.res <= w_res[0+:DATA_W];
        MULH:     o_exe_res.res <= w_res[DATA_W+:DATA_W];
        default:  o_exe_res.res <= '0;
      endcase
    end
    else begin
      o_exe_res_rdy <= '0;
    end
  end

//------------------------------------------------------------------------------
//---- functions
//------------------------------------------------------------------------------
endmodule
