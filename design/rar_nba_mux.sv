//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
// non-blocking asymmetrical mux that commutes input str to a respective unbound output str
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module rar_nba_mux #(
    parameter type  CH_T = logic,         //channels type
    parameter       NOIC = 'd4,           //number of input channels
    parameter       NOOC = 'd8            //number of output channels
)(
    input   logic   i_arr_ch_rdy [NOIC],
    input   CH_T    i_arr_ch [NOIC],
    output  logic   o_arr_ch_rdy [NOOC],
    output  CH_T    o_arr_ch [NOOC],
    output  logic   o_err                 //several input channels request same output channel
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    logic [NOIC-1:0]  v_mux_sel [NOOC];
    logic [NOIC-1:0]  v_ch_rqst [NOOC];
    logic             v_err [NOOC];

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    assign o_err = ~v_err.and;

    generate
        for (genvar gen_out_indx = 0; gen_out_indx < NOOC; gen_out_indx++) begin : noe_mux_gen
            //----
            assign v_err [gen_out_indx] = !$onehot(v_ch_rqst [gen_out_indx]);
            //----
            always_comb begin : rqst_detector
                for (int i = 0; i < NOIC; i++) begin
                    if (i_arr_ch[i].tag == gen_out_indx && i_arr_ch_rdy[i])
                        v_ch_rqst [gen_out_indx][i]  = '1;
                    else
                        v_ch_rqst [gen_out_indx][i]  = '0;
                end
            end
            //----
            always_comb begin : decoder
                for (int i = 0; i < NOIC; i++) begin
                    if ((i == 0 && v_ch_rqst [gen_out_indx][i])
                        || (i > 0 && v_ch_rqst [gen_out_indx][i] && !v_ch_rqst [gen_out_indx][i-1])) begin
                        v_mux_sel [gen_out_indx][i]  = '1;
                    end
                    else begin
                        v_mux_sel [gen_out_indx][i]  = '0;
                    end
                end
            end
            //----
            always_comb begin : multiplexor
                if (|v_mux_sel [gen_out_indx]) begin
                    for (int i = 0; i < NOIC; i++) begin
                        if (v_mux_sel [gen_out_indx][i]) begin
                            o_arr_ch_rdy[gen_out_indx]   = i_arr_ch_rdy[i];
                            o_arr_ch[gen_out_indx]       = i_arr_ch[i];
                        end
                    end
                end
                else begin
                    o_arr_ch_rdy[gen_out_indx]   = '0;
                end
            end
        end
    endgenerate

endmodule
