//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
// all project parameters combination
//------------------------------------------------------------------------------
package mpca04sc16_pkg;
    `include "cfg.sv"      //main configuration
    `include "types.sv"  //general parameters and data types
    `include "mm.sv"       //memory map
endpackage
