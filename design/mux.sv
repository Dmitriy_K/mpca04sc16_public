//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
// a non-blocking combinatorial, asymmetrical dma if mux
// sub[NOSUB] <-> src[NOSRC]
//----
import mpca04sc16_pkg::*;
module mux #(
  parameter         NOSUB               = 'd1,  //number of subscribers channels
  parameter         NOSRC               = 'd2,  //number of sources channels
  parameter integer SRC_SIZE   [NOSRC]  = '{default:0},
  parameter addr_t  SRC_B_ADDR [NOSRC]  = '{default:0}
)(
  input   wire                i_rst_n,
  input   wire                i_clk,
  input   dma_ch_rqst_struct  i_sub_rqst  [NOSUB],
  output  dma_ch_reply_struct o_sub_reply [NOSUB],
  output  dma_ch_rqst_struct  o_src_rqst  [NOSRC],
  input   dma_ch_reply_struct i_src_reply [NOSRC]
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------
const dma_ch_rqst_struct  DFLT_SRC_RQST = '{rqst:'0, we:'0, addr:'0, data:'0};

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    integer             c_cur_pos [NOSRC];
    integer             c_cntr [NOSRC];
    logic [NOSUB-1:0]   c_mux_sel [NOSRC];
    logic [NOSUB-1:0]   c_ch_rqst [NOSRC];
    logic               fl_pos_found [NOSRC];
    addr_t              c_src_addr_ptrn [NOSRC];
    addr_t              c_sub_addr_ptrn [NOSRC][NOSUB];

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    //----
    generate
        for (genvar gen_src_indx = 0; gen_src_indx < NOSRC; gen_src_indx++) begin : sub_addr_parser
            for (genvar gen_sub_indx = 0; gen_sub_indx < NOSUB; gen_sub_indx++) begin
                assign c_sub_addr_ptrn[gen_src_indx][gen_sub_indx] = i_sub_rqst[gen_sub_indx].addr[$left(addr_t) : $clog2(SRC_SIZE[gen_src_indx])];
            end
        end
    endgenerate

    //----
    generate
        for (genvar gen_src_indx = 0; gen_src_indx < NOSRC; gen_src_indx++) begin : src_addr_parser
            assign c_src_addr_ptrn[gen_src_indx] = SRC_B_ADDR[gen_src_indx][$left(addr_t) : $clog2(SRC_SIZE[gen_src_indx])];
        end
    endgenerate

    //----
    generate
        for (genvar gen_src_indx = 0; gen_src_indx < NOSRC; gen_src_indx++) begin : rqst_gen
            //----
            always_comb begin : rqst_detector
                for (int sub_indx = 0; sub_indx < NOSUB; sub_indx++) begin
                    if (i_sub_rqst[sub_indx].rqst && c_sub_addr_ptrn[gen_src_indx][sub_indx] == c_src_addr_ptrn[gen_src_indx])
                        c_ch_rqst [gen_src_indx][sub_indx]  = '1;
                    else
                        c_ch_rqst [gen_src_indx][sub_indx]  = '0;
                end
            end
        end
    endgenerate

    //----
    always_comb begin : multiplexor
        for (int src_indx = 0; src_indx < NOSRC; src_indx++) begin
            for (int sub_indx = 0; sub_indx < NOSUB; sub_indx++) begin
                if (c_ch_rqst [src_indx][sub_indx]
                    && (!c_ch_rqst [src_indx][c_cur_pos[src_indx]]
                        || sub_indx == c_cur_pos[src_indx])) begin
                    o_src_rqst[src_indx]    = i_sub_rqst[sub_indx];
                    o_sub_reply[sub_indx]   = i_src_reply[src_indx];
                    fl_pos_found[src_indx]  = '1;
                    break;
                end
                else begin
                    o_src_rqst[src_indx]    = '{default:0};
                    fl_pos_found[src_indx]  = '0;
                end
            end
        end
    end

    //----
    generate
        for (genvar gen_src_indx = 0; gen_src_indx < NOSRC; gen_src_indx++) begin : position_reg_gen
            //----
            always_ff @ (posedge i_clk or negedge i_rst_n) begin : position_reg
                if (!i_rst_n)
                    c_cur_pos[gen_src_indx] <= 'd0;
                else if (fl_pos_found[gen_src_indx])
                    c_cur_pos[gen_src_indx] <= c_cntr[gen_src_indx];
            end
        end
    endgenerate

endmodule
