//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
import prj_func::wr_32b_rw_reg;
module mem_sr (
    //---- system
    input  wire     i_rst_n,
    input  wire     i_clk,
    //---- register i/f
    input  wire     i_rqst,
    input  wen_t    i_we,
    input  addr_t   i_addr,
    input  data_t   i_data,
    output data_t   o_data,
    output data_t   o_arr_sr [MEM_NOSYSR]
);
//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------
    localparam P0_INDX = integer'(P0_ADDR[MEM_SR_ADDR_HIGH_CUT:MEM_SR_ADDR_LOW_CUT]);
    localparam SP_INDX = integer'(SP_ADDR[MEM_SR_ADDR_HIGH_CUT:MEM_SR_ADDR_LOW_CUT]);

//------------------------------------------------------------------------------
//---- wires
//------------------------------------------------------------------------------
    data_t  mem_sr [MEM_NOSYSR];
    wen_t   v_wr_strb;
    logic   v_rd_strb;
    addr_t  w_reg_indx;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    assign v_wr_strb    = (i_rqst) ? i_we : '0;
    assign v_rd_strb    = (i_we == 0 && i_rqst) ? '1 : '0;
    assign w_reg_indx   = integer'(i_addr[MEM_SR_ADDR_HIGH_CUT:MEM_SR_ADDR_LOW_CUT]);
    assign o_arr_sr     = mem_sr;

//------------------------------------------------------------------------------
//---- register rd operations
//------------------------------------------------------------------------------
    always @(*) begin: s_rd_mux
        if (v_rd_strb) begin
            for (int i = 0; i < MEM_NOSYSR; i++) begin
                if (w_reg_indx == i) begin
                    o_data  = mem_sr[i];
                    break;
                end
            end
        end
    end

//------------------------------------------------------------------------------
//---- register wr operations
//------------------------------------------------------------------------------
    always @(posedge i_clk or negedge i_rst_n) begin: s_wr_regs
        if (!i_rst_n) begin
            // todo - here should be left only those system registers which must be set at the reset signal
            // mem_sr[P0_INDX] <= '0;
            // mem_sr[SP_INDX] <= '0;
            mem_sr <= '{default:'0};
        end
        else begin
            for (int i = 0; i < MEM_NOSYSR; i++) begin
                if (w_reg_indx == i) begin
                    mem_sr[i] = wr_32b_rw_reg(v_wr_strb, mem_sr[i], i_data, '1);
                    // todo - check why Verilator does not support non-blocking assignment in this case
                    // mem_sr[i] <= wr_32b_rw_reg(v_wr_strb, mem_sr[i], i_data, '1);
                    break;
                end
            end
        end
    end

endmodule
