//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module rar_str #(
    integer PN = 0
)(
    input   wire            i_rst_n,
    input   wire            i_clk,
    input   wire            i_ignore_new_str,
    //----
    input   wire            i_new_str_rdy,
    input   rar_str_struct  i_new_str,
    //----
    input   wire            i_res_rdy,
    input   exe_res_struct  i_res,
    //----
    input   rar_str_struct  i_arr_str [BUF_SIZE],
    output  rar_str_struct  o_str,
    //----
    output  reg             o_err_usst //unknown str state
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    rar_str_struct w_arg0;
    rar_str_struct w_arg1;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    //---- manipulation with string
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : str_service
        if (!i_rst_n) begin
            o_str.st    <= EMPTY;
            o_err_usst  <= '0;
        end
        else begin
            case (o_str.st)
                EMPTY, RES : begin
                    if (!i_ignore_new_str && i_new_str_rdy)
                        o_str <= i_new_str;
                end
                WAIT : begin
                    if (o_str.ss_arg0 == RAR && w_arg0.st == RES) begin
                        o_str.arg0      <= w_arg0.arg1_res;
                        o_str.ss_arg0   <= NA_SRC;
                    end
                    if (o_str.ss_arg1 == RAR && w_arg1.st == RES) begin
                        o_str.res_fl    <= w_arg1.res_fl;
                        o_str.arg1_res  <= w_arg1.arg1_res;
                        o_str.ss_arg1   <= NA_SRC;
                    end
                    else if (o_str.ss_arg1 == CALC && w_arg0.st == RES) begin
                        o_str.arg1_res  <= o_str.arg1_res + w_arg0.arg1_res;
                        o_str.ss_arg1   <= NA_SRC;
                    end
                    //----
                    if (w_arg0.st == RES && w_arg1.st == RES)
                        o_str.st        <= SET;
                end
                SET : begin
                    if (i_res_rdy && i_res.tag == PN) begin
                        o_str.st        <= RES;
                        o_str.res_fl    <= i_res.fl;
                        o_str.arg1_res  <= i_res.res;
                    end
                end
                default : begin
                    o_str.st    <= EMPTY;
                    o_err_usst  <= '1;
                end
            endcase
        end
    end

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
    rar_str_mux arg0_mux (
        .i_tag      (integer'(o_str.arg0)),
        .i_arr_str  (i_arr_str),
        .o_str      (w_arg0)
    );
    //----
    rar_str_mux arg1_mux (
        .i_tag      (integer'(o_str.arg1_res)),
        .i_arr_str  (i_arr_str),
        .o_str      (w_arg1)
    );

endmodule
