//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
// todo - add exceptions for the situations where arg of cmd is linking to 0 rar address
// todo - add exceptions for arg0 when cmd is linking to 0, let it be the sign of cg source instead of rar
//----
import mpca04sc16_pkg::*;
module du #(
    int PN = 'd0
)(
    input  wire             i_rst_n,
    input  wire             i_clk,
    input  logic            i_stop_decode,                  // rar is full
    output logic            o_stop_fetch,
    input  integer          i_tag,
    input  data_t           i_mem [MEM_NOSYSR],             // system regs only
    //---- fu interconnect
    input  logic            i_fu_instr_rdy,                 // Instruction word is ready
    input  integer          i_fu_frmt,                      // instruction format
    input  logic            i_fu_eolcs,                     // eols (end of a line code section)
    input  brake_pnt_t      i_fu_bp,                        // bp
    input  data_t           i_fu_instr,                     // Raw instruction word
    //---- rar interconnect
    output logic            o_rar_str_rdy,
    output rar_str_struct   o_rar_str
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    integer     v_am_ss_arg1;
    operation_t v_operation;
    am_t        v_am_arg1;
    am_t        v_am_ref_base_arg1;
    am_t        v_am_bias_arg1;
    ss_t        v_ss_arg0;
    ss_t        v_ss_arg1;
    ss_t        v_ss_ref_base_arg1;
    ss_t        v_ss_bias_arg1;
    data_t      v_f0;
    data_t      v_f1;

    logic   v_enable_decoding;
    logic   v_arg0_rdy, v_arg1_rdy;
    tag_t   v_tag;
    tag_t   v_rar_addr_f0, v_rar_addr_f1;
    addr_t  v_mem_addr_ref_base_arg1;
    data_t  v_cg_data_arg1, v_cg_data_bias_arg1;
    data_t  v_mem_data_ref_base_arg1;
    data_t  v_arg0, v_arg1;
    ss_t    v_curr_ss_arg1;
    exe_t   v_exn;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    //---- predecoding, recognition of obvious command parts
    assign v_enable_decoding = i_fu_instr_rdy & i_rst_n;
    assign v_am_ss_arg1 =   (v_enable_decoding && i_fu_frmt == 1) ?
                                integer'(i_fu_instr[AM_SS_ARG1_POS_E:AM_SS_ARG1_POS_B]) :
                                'd0;
    assign v_operation =    (v_enable_decoding && i_fu_frmt == 0) ?
                                operation_t'(i_fu_instr[OP_FRMT0_POS_E:OP_FRMT0_POS_B]) :
                            (v_enable_decoding && i_fu_frmt == 1) ?
                                operation_t'({2'b00, i_fu_instr[OP_FRMT1_POS_E:OP_FRMT1_POS_B]}) :
                            (v_enable_decoding && i_fu_frmt == 2) ?
                                operation_t'(LDC) :
                                '0;
    assign v_f0 =   (v_enable_decoding && i_fu_frmt == 0) ?
                        data_t'(i_fu_instr[F0_FRMT0_POS_E:F0_FRMT0_POS_B]) :
                    (v_enable_decoding && i_fu_frmt == 1) ?
                        data_t'(i_fu_instr[F0_FRMT1_POS_E:F0_FRMT1_POS_B]) :
                        '0;
    assign v_f1 =   (v_enable_decoding && i_fu_frmt == 0) ?
                        data_t'(i_fu_instr[F1_FRMT0_POS_E:F1_FRMT0_POS_B]) :
                    (v_enable_decoding && i_fu_frmt == 1) ?
                        data_t'(i_fu_instr[F1_FRMT1_POS_E:F1_FRMT1_POS_B]) :
                    (v_enable_decoding && i_fu_frmt == 2) ?
                        data_t'(i_fu_instr[F1_FRMT2_POS_E:F1_FRMT2_POS_B]) :
                        '0;

    //----
    always_comb begin : pre_decoder
        if (v_enable_decoding) begin
            case (i_fu_frmt)
                'd0: begin
                    v_am_arg1           = DIR;
                    v_am_ref_base_arg1  = NA_AM;
                    v_am_bias_arg1      = NA_AM;
                    v_ss_arg0           = (v_f0 > 0) ? RAR : NA_SRC;
                    v_ss_arg1           = ss_t'(i_fu_instr[SS_ARG1_POS]);
                    v_ss_ref_base_arg1  = NA_SRC;
                    v_ss_bias_arg1      = NA_SRC;
                end
                'd1: begin
                    v_am_arg1           = (v_am_ss_arg1 inside {0, 1}) ? DIR :
                                            (v_am_ss_arg1 inside {2, 3}) ? DISP :
                                                NA_AM; //it's an error
                    v_am_ref_base_arg1  = (v_am_ss_arg1 inside {2, 3}) ? DIR : NA_AM;
                    v_am_bias_arg1      = (v_am_ss_arg1 inside {2, 3}) ? DIR : NA_AM;
                    v_ss_arg0           = (i_fu_instr[OP_FRMT1_POS_E] == 1) ? RAR : NA_SRC;
                    v_ss_arg1           = (v_am_ss_arg1 == 0) ? RAR :
                                            (v_am_ss_arg1 == 1) ? CG :
                                                (v_am_ss_arg1 inside {2, 3}) ? CALC :
                                                    NA_SRC; //it's an error
                    v_ss_ref_base_arg1  = (v_am_ss_arg1 == 2) ? RAR :
                                            (v_am_ss_arg1 == 3) ? MEM :
                                                NA_SRC;
                    v_ss_bias_arg1      = (v_am_ss_arg1 inside {2, 3}) ? CG : NA_SRC;
                end
                'd2: begin
                    v_am_arg1           = DIR;
                    v_am_ref_base_arg1  = NA_AM;
                    v_am_bias_arg1      = NA_AM;
                    v_ss_arg0           = NA_SRC;
                    v_ss_arg1           = CG;
                    v_ss_ref_base_arg1  = NA_SRC;
                    v_ss_bias_arg1      = NA_SRC;
                end
                default: begin //it's an error
                    v_am_arg1           = NA_AM;
                    v_am_ref_base_arg1  = NA_AM;
                    v_am_bias_arg1      = NA_AM;
                    v_ss_arg0           = NA_SRC;
                    v_ss_arg1           = NA_SRC;
                    v_ss_ref_base_arg1  = NA_SRC;
                    v_ss_bias_arg1      = NA_SRC;
                end
            endcase
        end
    end

    // preparing rar string components
    assign v_tag = (i_tag + PN) % BUF_SIZE;

    //---- rar addresses corrections
    assign v_rar_addr_f0 = tag_t'(((v_tag + BUF_SIZE) - v_f0) % BUF_SIZE);
    assign v_rar_addr_f1 = tag_t'(((v_tag + BUF_SIZE) - v_f1) % BUF_SIZE);

    //---- addr mux
    // todo - add here selection for SYS_REG_SP for PUSH/POP cmds
    //  and recognition or RD and WR commands
    // assign v_mem_addr_ref_base_arg1 = SP_ADDR;
    assign v_mem_addr_ref_base_arg1 = v_f1;

    //---- mem mux
    assign v_mem_data_ref_base_arg1 = (v_ss_ref_base_arg1 == MEM) ?
                                    i_mem[v_mem_addr_ref_base_arg1] :
                                        v_mem_data_ref_base_arg1;

    //---- cg muxs
    assign v_cg_data_arg1      = (v_ss_arg1 == CG) ? v_f1 : v_cg_data_arg1;
    assign v_cg_data_bias_arg1 = (v_ss_bias_arg1 == CG) ? v_f1 : v_cg_data_bias_arg1;

    //---- decode execution unit
    always_comb begin : exn_decoder
        unique if (v_operation >= 'b11001 || i_fu_frmt == 'd2) begin
            v_exn = CU;
        end
        else if (v_operation inside{MULH, MULL}) begin
            v_exn = MUL;
        end
        else if (v_operation <= 'b00101) begin
            v_exn = MAC;
        end
        else begin
            v_exn = ALU;
        end
    end

    //---- rar string composer
    //---- figure out if arguments are ready at the decoding stage
    assign v_arg0_rdy = (v_ss_arg0 == NA_SRC) ? '1 : '0;
    assign v_arg1_rdy = (v_am_arg1 == DIR || (v_am_arg1 == DISP && v_ss_ref_base_arg1 == MEM)) ? '1 : '0;

    //----
    assign v_arg0 = (v_ss_arg0 == NA_SRC) ? '0 : data_t'(v_rar_addr_f0);

    //----
    always_comb begin
        if(v_am_arg1 == DIR) begin
            if (v_ss_arg1 == CG) begin
                v_arg1          = v_cg_data_arg1;
                v_curr_ss_arg1  = NA_SRC;
            end
            else if (v_ss_arg1 == RAR) begin
                v_arg1          = data_t'(v_rar_addr_f1);
                v_curr_ss_arg1  = RAR;
            end
            else begin
                v_arg1          = v_arg1;
                v_curr_ss_arg1  = NA_SRC;
            end
        end
        else if (v_am_arg1 == DISP) begin
            if (v_ss_ref_base_arg1 == MEM) begin
                v_arg1          = v_mem_data_ref_base_arg1 + v_cg_data_bias_arg1;
                v_curr_ss_arg1  = NA_SRC;
            end
            else if (v_ss_ref_base_arg1 == RAR) begin
                v_arg1          = v_cg_data_bias_arg1;
                v_curr_ss_arg1  = CALC;
            end
            else begin
                // it is an error state
                v_arg1          = v_arg1;
                v_curr_ss_arg1  = NA_SRC;
            end
        end
        else begin
            // it is an error state
            v_arg1          = v_arg1;
            v_curr_ss_arg1  = NA_SRC;
        end
    end

    //----
    // st       - str status
    // tag      - a tag (it's necessary if a str keeps a tag)
    // exn      - executor number
    // op       - operation
    // eol      - eolcs
    // bp       - break point sign
    // ss_arg0  - source selector (rar or none)
    // ss_arg1  - source selector (cg, rar or calc or none)
    // res_fl      - condition or flags of result (the source for flags to compare is always a previous accomplished command)
    // arg0     - an argument or result address (if it is not ready on the current stage)
    // arg1/res - an argument or result address (if it is not ready on the current stage) or result of operation
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : str_composer
        if (!i_rst_n) begin
            o_rar_str.st        <= EMPTY;
        end
        else if (i_fu_instr_rdy && !i_stop_decode) begin
            o_rar_str.st        <= (v_arg0_rdy && v_arg1_rdy) ? SET : WAIT;
            o_rar_str.tag       <= v_tag;
            o_rar_str.exn       <= v_exn;
            o_rar_str.op        <= v_operation;
            o_rar_str.eol       <= i_fu_eolcs;
            o_rar_str.bp        <= i_fu_bp;
            o_rar_str.ss_arg0   <= v_ss_arg0;
            o_rar_str.ss_arg1   <= v_curr_ss_arg1;
            o_rar_str.res_fl    <= NA_CND;
            o_rar_str.arg0      <= v_arg0;
            o_rar_str.arg1_res  <= v_arg1;
        end
    end

    //----
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : rdy_generator
        if (!i_rst_n)
            o_rar_str_rdy <= '0;
        else if (!i_stop_decode)
            o_rar_str_rdy <= i_fu_instr_rdy;
        else if (i_stop_decode && i_fu_instr_rdy && !o_rar_str_rdy)
            o_rar_str_rdy <= '1;
        else
            o_rar_str_rdy <= o_rar_str_rdy;
    end

    //----
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : stop_fetch
        if (!i_rst_n)  o_stop_fetch <= '0;
        else           o_stop_fetch <= i_stop_decode;
    end

endmodule
