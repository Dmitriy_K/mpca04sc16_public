//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
// a round-robin searching algorithm for scaning RAR requests
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module rar_exe_mux #(
    parameter exe_t EXE_TYPE = ALU
)(
    input  wire             i_rst_n,
    input  wire             i_clk,
    input  rar_str_struct   i_arr_str [BUF_SIZE],
    input  logic            i_exe_res_rdy,
    output logic            o_exe_rqst,
    output exe_task_struct  o_exe_task
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    integer v_cur_pos;
    integer v_cntr;
    logic   fl_pos_found;
    logic   v_arr_rqst [BUF_SIZE];

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    assign o_exe_rqst   = fl_pos_found;

    //----
    generate
        for (genvar gen_str_indx = 0; gen_str_indx < BUF_SIZE; gen_str_indx++) begin : rqst_arr_former
            assign v_arr_rqst [gen_str_indx] = (i_arr_str[gen_str_indx].exn == EXE_TYPE && i_arr_str[gen_str_indx].st == SET);
        end
    endgenerate

    //----
    // fixme - according to last modification in mux.sv
    always_comb begin
        for (v_cntr = v_cur_pos; v_cntr < (BUF_SIZE-1); v_cntr++) begin
            if (v_arr_rqst[v_cntr]) begin
                o_exe_task.tag  = i_arr_str[v_cntr].tag;
                o_exe_task.op   = i_arr_str[v_cntr].op;
                o_exe_task.eol  = i_arr_str[v_cntr].eol;
                o_exe_task.bp   = i_arr_str[v_cntr].bp;
                o_exe_task.arg0 = i_arr_str[v_cntr].arg0;
                o_exe_task.arg1 = i_arr_str[v_cntr].arg1_res;
                fl_pos_found    = '1;
                break;
            end
            else begin
                fl_pos_found    = '0;
            end
        end
    end

    //----
    always_ff @ (posedge i_clk or negedge i_rst_n) begin : priority_service
        if (!i_rst_n)           v_cur_pos <= 'd0;
        else if (fl_pos_found)  v_cur_pos <= v_cntr;
        else                    v_cur_pos <= 'd0;
    end

endmodule
