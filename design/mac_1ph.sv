//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
// 1 phase argument channel, it supports only direct addressing mode
// appropriate for command and argument fetching
// it completes transaction anyway even if request has gone
// data is kept on the out until the next value is ready
// ! initial i_mac_rdy state must be active
//------------------------------------------------------------------------------
module mac_1ph #(
  parameter int   WEN_W     = 'd1,
  parameter logic MODE      = '1,  // 0 - one shot, 1 - continuous
  parameter type  ADDR_BUS_T = logic,
  parameter type  DATA_BUS_T = logic
)
(
  //---- sys
  input  wire               i_rst_n,
  input  wire               i_clk,
  input  logic              i_en,
  input  logic              i_hold,
  input  logic              i_flush,
  //---- ctrl
  input  logic              i_rqst,
  input  logic [WEN_W-1:0]  i_we,
  output logic              o_rdy,
  output logic              o_raw_rdy,   //copy of i_mac_rdy
  input  integer            i_id,
  output integer            o_id,
  input  ADDR_BUS_T         i_addr_inc,
  input  DATA_BUS_T         i_data,
  output DATA_BUS_T         o_data,
  //---- memory access channel
  output logic              o_mac_rqst,
  output logic [WEN_W-1:0]  o_mac_we,
  input  logic              i_mac_rdy,
  output ADDR_BUS_T         o_mac_addr,
  input  DATA_BUS_T         i_mac_data,
  output DATA_BUS_T         o_mac_data
);
//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------
typedef enum {IDLE, INIT, ACCESS} st_fsm_t;
localparam integer ADDR_FIFO_DEPTH = 'd2;
localparam integer ID_SIZE = 'd32;

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
st_fsm_t st_fsm;
logic unsigned [ID_SIZE*ADDR_FIFO_DEPTH-1:0] c_mac_id;

//------------------------------------------------------------------------------
//---- logic
assign o_raw_rdy  = i_mac_rdy;
//----
always_ff @ (posedge i_clk or negedge i_rst_n) begin : fsm
  if (!i_rst_n) begin
    st_fsm <= IDLE;
    set_dflt();
  end
  else begin
    case (st_fsm)
      IDLE : begin
        if (is_en() && is_rqst()) begin
          st_fsm <= INIT;
          set_new_rqst();
        end
      end
      INIT : begin
        if (!is_en()) begin
          st_fsm <= IDLE;
          set_dflt();
        end
        else if (!is_hold()) begin
          if (MODE && is_rqst()) set_new_rqst();
          st_fsm <= ACCESS;
        end
      end
      ACCESS  : begin
        unique if (!is_en()) begin
          st_fsm <= IDLE;
          set_dflt();
        end
        else if (is_flush()) begin
          st_fsm <= INIT;
          set_new_rqst();
          o_rdy  <= '0;
        end
        else if (is_hold()) begin
          st_fsm <= INIT;
          o_rdy  <= '0;
        end
        else if (i_mac_rdy) begin
          if (MODE && is_rqst()) begin
            set_new_rqst();
          end
          else begin
            st_fsm <= IDLE;
            set_dflt();
          end
          set_new_result();
        end
        else begin
          o_rdy <= '0;
        end
      end
      default: begin
        st_fsm <= IDLE;
        set_dflt();
      end
    endcase
  end
end

//------------------------------------------------------------------------------
//---- task and functions
//------------------------------------------------------------------------------
task set_dflt;
    o_rdy       <= '0;
    o_mac_rqst  <= '0;
endtask
//----
task set_new_rqst();
    o_mac_rqst  <= i_rqst;
    o_mac_we    <= i_we;
    o_mac_addr  <= i_addr_inc;
    o_mac_data  <= (i_we != '0) ? i_data : o_mac_data;
    c_mac_id    <= {>> ID_SIZE {c_mac_id[ID_SIZE*(ADDR_FIFO_DEPTH-1)-1:0], i_id}};
endtask
//----
task set_new_result;
    o_rdy   <= '1;
    o_data  <= i_mac_data;
    o_id    <= c_mac_id[ID_SIZE*ADDR_FIFO_DEPTH-1 -: ID_SIZE];
endtask
//----
function logic is_en;
    is_en = (i_en) ? '1 : '0;
endfunction
//----
function logic is_hold;
    is_hold = (i_hold) ? '1 : '0;
endfunction
//----
function logic is_flush;
    is_flush = (i_flush) ? '1 : '0;
endfunction
//----
function logic is_rqst;
    is_rqst = (i_rqst) ? '1 : '0;
endfunction

endmodule