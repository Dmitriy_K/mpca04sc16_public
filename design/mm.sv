//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
parameter MEM_NOSYSR = 'd16;                            //number of system registers
parameter addr_t START_ADDR    = '0;
//---- memory map
parameter MEM_MM_BLK_SIZE = 'd2**12;
parameter MEM_SR_BLK_SIZE = MEM_NOSYSR*BIDW;
parameter addr_t MEM_MM_B_ADDR   = 'h0000;              // main memory base address
parameter addr_t MEM_SR_B_ADDR   = 'hF000;              // system regs base address
//---- system registers
localparam MEM_SR_ADDR_HIGH_CUT = $clog2(MEM_SR_BLK_SIZE);  // highest address bit for SR registers
localparam MEM_SR_ADDR_LOW_CUT = $clog2(BIDW);              // number of ignored low bits
parameter addr_t P0_ADDR = MEM_SR_B_ADDR + BIDW*'d0;    // pointers
parameter addr_t SP_ADDR = MEM_SR_B_ADDR + BIDW*'d15;   // stack pointer
//---- the cpu internal memory bus
parameter SUB_CN_MAC    = 'd0;
parameter SRC_CN_MM     = 'd0;
parameter SRC_CN_SR     = 'd1;