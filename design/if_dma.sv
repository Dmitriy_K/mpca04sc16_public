//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------

interface if_dma import prj_pkg::*; (
  input wire rst_n, clk
);
                                 // mst  | slv
                                 //------+------
  wire              rdy;         // i    | o
  wire              cen;         // o    | i
  wire [WEN_W-1:0]  wen;         // o    | i
  wire [ADDR_W-1:0] addr;        // o    | i
  wire [DATA_W-1:0] data_out;    // o    | i
  wire [DATA_W-1:0] data_in;     // i    | o

  //----
  reg               r_rdy;
  reg               r_cen;
  reg [WEN_W-1:0]   r_wen;
  reg [ADDR_W-1:0]  r_addr;
  reg [DATA_W-1:0]  r_data_in;
  reg [DATA_W-1:0]  r_data_out;

  //----
  assign rdy      = r_rdy;
  assign cen      = r_cen;
  assign wen      = r_wen;
  assign addr     = r_addr;
  assign data_in  = r_data_in;
  assign data_out = r_data_out;

  //----
  modport mst (
      input  rdy,
      output cen,
      output wen,
      output addr,
      output data_out,
      input  data_in
    );

  //----
  modport slv (
      output  rdy,
      input   cen,
      input   wen,
      input   addr,
      input   data_out,
      output  data_in
    );

endinterface
