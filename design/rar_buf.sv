//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
import mpca04sc16_pkg::*;
module rar_buf (
    input  wire             i_rst_n,
    input  wire             i_clk,
    output logic            o_full,
    input  wire             i_new_str_rdy [NOC],
    input  rar_str_struct   i_new_str [NOC],
    input  wire             i_arr_res_rdy [BUF_SIZE],
    input  exe_res_struct   i_arr_res [BUF_SIZE],
    output rar_str_struct   o_arr_str [BUF_SIZE]
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
    logic           v_str_engaged [BUF_SIZE];
    wire            arr_muxed_str_rdy [BUF_SIZE];
    rar_str_struct  arr_muxed_str [BUF_SIZE];

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
    assign o_full = v_str_engaged.and;

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
    rar_nba_mux #(
        .CH_T   (rar_str_struct),
        .NOIC   (NOC),
        .NOOC   (BUF_SIZE)
    )
    new_str_mux (
        .i_arr_ch_rdy   (i_new_str_rdy),
        .i_arr_ch       (i_new_str),
        .o_arr_ch_rdy   (arr_muxed_str_rdy),
        .o_arr_ch       (arr_muxed_str),
        .o_err          ()
    );

    //----
    generate
        for (genvar gen_str_indx = 0; gen_str_indx < BUF_SIZE; gen_str_indx++) begin : rar_str_gen
            //----
            assign v_str_engaged[gen_str_indx] = (o_arr_str[gen_str_indx].st inside {WAIT, SET});
            //----
            rar_str #(
                .PN               (gen_str_indx)
            )
            rar_str (
                .i_rst_n          (i_rst_n),
                .i_clk            (i_clk),
                .i_ignore_new_str (o_full),
                .i_new_str_rdy    (arr_muxed_str_rdy[gen_str_indx]),
                .i_new_str        (arr_muxed_str[gen_str_indx]),
                .i_res_rdy        (i_arr_res_rdy[gen_str_indx]),
                .i_res            (i_arr_res[gen_str_indx]),
                .i_arr_str        (o_arr_str),
                .o_str            (o_arr_str[gen_str_indx]),
                .o_err_usst       ()
            );
        end
    endgenerate

endmodule
