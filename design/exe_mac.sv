//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
module exe_mac import prj_pkg::*; (
  input  wire                 i_rst_n,
  input  wire                 i_clk,
  //---- rar interconnect
  input  wire                 i_exe_task_rqst,
  input  exe_task_struct      i_exe_task,
  output logic                o_exe_res_rdy,
  output exe_res_struct       o_exe_res,
  //---- memory access channel
  input  wire                 i_dma_rdy,
  output reg                  o_dma_rqst,
  output reg  [WEN_W-1:0]     o_dma_we,
  output reg  [ADDR_W-1:0]    o_dma_addr,
  output reg  [DATA_W-1:0]    o_dma_data,
  input  wire [DATA_W-1:0]    i_dma_data
);

//------------------------------------------------------------------------------
//---- const
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//---- vars
//------------------------------------------------------------------------------
logic               c_task_rqst;
logic [WEN_W-1:0]   w_wr_msk;
logic               w_mac_rdy;
logic [ADDR_W-1:0]  w_addr;
logic [DATA_W-1:0]  w_data, w_mac_data;
logic [DATA_W-1:0]  w_res;
integer             w_mac_tag;

//------------------------------------------------------------------------------
//---- logic
//------------------------------------------------------------------------------
// data type      | rd    | op
// data size      | rd/wr | op
// memory address | rd/wr | arg1
// data in	      | wr    | arg0
// data out	      | rd    | res
  assign w_addr         = i_exe_task.arg1;
  assign w_data         = i_exe_task.arg0;
  assign c_task_rqst    = (i_rst_n & i_exe_task_rqst);
  assign w_wr_msk       = (i_exe_task.op == STB) ?  'd1 :
                          (i_exe_task.op == ST2B) ? 'd3 :
                                                    '0;
  assign w_res          = (i_exe_task.op == LDSB && w_mac_data[7]) ? {'1, w_mac_data[7:0]} : w_mac_data;
  assign o_exe_res.tag  = TAG_W'(w_mac_tag);
  assign o_exe_res_rdy  = w_mac_rdy;
  assign o_exe_res.cnd  = (w_mac_data == 0 && w_mac_rdy && w_wr_msk == 0) ? Z : NA_CND;
  assign o_exe_res.res  = (i_exe_task.op inside {LDUB, LDSB, LDU2B, LDS2B}) ? w_res : i_exe_task.arg1;

//------------------------------------------------------------------------------
//---- inst
//------------------------------------------------------------------------------
  mac_1ph #(
    .WEN_W            (WEN_W),
    .ADDR_W           (ADDR_W),
    .MODE             ('0),
    .DATA_CH_T        (logic [DATA_W-1:0])
  )
  rd_mac (
    .i_rst_n          (i_rst_n),
    .i_clk            (i_clk),
    .i_en             ('1),
    .i_hold           ('0),
    .i_flush          ('0),
    .i_rqst           (c_task_rqst),
    .i_we             (w_wr_msk),
    .o_rdy            (w_mac_rdy),
    .o_raw_rdy        (),
    .i_id             (integer'(i_exe_task.tag)),
    .o_id             (w_mac_tag),
    .i_addr_inc       (w_addr),
    .i_data           (w_data),
    .o_data           (w_mac_data),
    .o_mac_rqst       (o_dma_rqst),
    .o_mac_we         (o_dma_we),
    .i_mac_rdy        (i_dma_rdy),
    .o_mac_addr       (o_dma_addr),
    .i_mac_data       (i_dma_data),
    .o_mac_data       (o_dma_data)
  );

//------------------------------------------------------------------------------
//---- functions
//------------------------------------------------------------------------------
endmodule
