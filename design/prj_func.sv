//------------------------------------------------------------------------------
//
// *  *   *  **   *    *   **   **
//    ** **  * *  *       *  *  * *
// *  * * *  * *  *    *  *     *  *
// *  * * *  * *  *    *  *     *  *
// *  * * *  **   * *  *  *  *  ****
// *  *   *  *    ***  *   **   *  *
// ----------
// mpca04sc16
//------------------------------------------------------------------------------
// general function
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---- calculates number of bytes that word width contains
//------------------------------------------------------------------------------
package prj_func;

  function integer bytes_in_word (input integer dw);
    if (dw < 'd8) begin
      return ('d1);
    end
    else begin
      if (dw % 'd8 != 'd0) return (dw / 'd8 + 1);
      else                 return (dw / 'd8);
    end
  endfunction

//------------------------------------------------------------------------------
//---- log2(x)
//---- for example, to calculate addr bus width if you know size in byte
//------------------------------------------------------------------------------
  function integer clogb2(input integer depth);
    integer result;
    //----
    begin
        result = 0;
        //----
        for (int vari = depth; vari > 0; vari = vari >> 1) begin
            result = result + 1;
          end
        //----
        clogb2 = result;
      end
  endfunction

//------------------------------------------------------------------------------
//--- function wr standart 32bit reg
//----  wr   - wr bus, individual line for each byte
//----  pre  - previous data
//----  cur  - current data to write
//----  msk  - msk for unused(reserved) bits
//------------------------------------------------------------------------------
  function [31:0] wr_32b_rw_reg (
    input [3:0]   wr,
    input [31:0]  pre,
    input [31:0]  cur,
    input [31:0]  msk
  );
    reg [31:0] result;
    //----
    begin
        if (wr[0]) result[7:0]   = cur[7:0]   & msk[7:0];   else result[7:0]   = pre[7:0];
        if (wr[1]) result[15:8]  = cur[15:8]  & msk[15:8];  else result[15:8]  = pre[15:8];
        if (wr[2]) result[23:16] = cur[23:16] & msk[23:16]; else result[23:16] = pre[23:16];
        if (wr[3]) result[31:24] = cur[31:24] & msk[31:24]; else result[31:24] = pre[31:24];
        //----
        wr_32b_rw_reg = result;
      end
  endfunction

//------------------------------------------------------------------------------
//--- function serve 32bit read-clear reg (fixs events and clear when read)
//----  rd  - rd event
//----  pre - previous(curent, old) value
//----  set - superposition of observed events and flags that the register fix
//----  msk - msk for unused(reserved) bits
//------------------------------------------------------------------------------
  function [31:0] serve_32b_rc_reg (
    input        rd,
    input [31:0] pre,
    input [31:0] set,
    input [31:0] msk
  );
    reg [31:0]  result;
    //----
    begin
        for (int i = 0; i < 32; i++)
          result[i] = (pre[i] & ~rd & msk[i]) || (set[i] & msk[i]);
        //----
        serve_32b_rc_reg = result;
      end
  endfunction

//------------------------------------------------------------------------------
//--- function serve 32bit read-clear reg (fixs events and clear when read)
//----  rd  - rd event
//----  pre - previous(curent, old) value
//----  set - superposition of bits that you want to set ('1' - turn bit onto '1')
//----  clr - superposition of bits that you want to clr ('1' - turn bit onto '0')
//----  msk - msk for unused(reserved) bits
//------------------------------------------------------------------------------
  function [31:0] wr_32b_sc_reg (
    input [3:0]   wr,
    input [31:0]  pre,
    input [31:0]  set,
    input [31:0]  clr,
    input [31:0]  msk
  );
    reg [31:0]  result;
    //----
    begin
        for (int i = 0; i < 8; i++) begin
            if (wr[0] && (clr[i]    || set[i])   ) result[i]    = set[i]    & msk[i];    else result[i]    = pre[i];
            if (wr[1] && (clr[8+i]  || set[8+i]) ) result[8+i]  = set[8+i]  & msk[8+i];  else result[8+i]  = pre[8+i];
            if (wr[2] && (clr[16+i] || set[16+i])) result[16+i] = set[16+i] & msk[16+i]; else result[16+i] = pre[16+i];
            if (wr[3] && (clr[24+i] || set[24+i])) result[24+i] = set[24+i] & msk[24+i]; else result[24+i] = pre[24+i];
          end
        //----
        wr_32b_sc_reg = result;
      end
  endfunction



endpackage
